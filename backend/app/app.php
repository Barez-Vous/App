<?php

use Symfony\Component\Translation\Loader\YamlFileLoader;
use Symfony\Component\Debug\ErrorHandler;
use Symfony\Component\Debug\ExceptionHandler;
use Symfony\Component\HttpFoundation\Request;

use Silex\Provider\SecurityServiceProvider;
use Silex\Provider\SecurityJWTServiceProvider;

use App\DAO\UserDao;
use App\DAO\BarDao;
use App\DAO\BarPropositionDao;
use App\DAO\CommentaireDao;
use App\Functions\GlobalFunctions;

use App\SimpleUserJWT\UserProvider;


ErrorHandler::register();
ExceptionHandler::register();

$app->register(new Silex\Provider\MonologServiceProvider(), array(
    'monolog.logfile' => '../logs/development.log',
));

$app->register(new Silex\Provider\DoctrineServiceProvider());
$app->register(new Silex\Provider\TranslationServiceProvider());
$app->extend('translator', function($translator, $app) {
	$translator->addLoader('yaml', new YamlFileLoader());
	$translator->addResource('yaml', __DIR__.'/locales/fr.yml', 'fr');
	return $translator;
});

$app['functions.global'] = $app->share(function ($app) {
	return new GlobalFunctions;
});

$app['dao.user'] = $app->share(function ($app) {
    return new UserDao($app['db']);
});
$app['dao.bar'] = $app->share(function ($app) {
	return new BarDao($app['db']);
});
$app['dao.barProposition'] = $app->share(function ($app) {
	return new BarPropositionDao($app['db']);
});
$app['dao.commentaire'] = $app->share(function ($app) {
	return new CommentaireDao($app['db']);
});


$app['user.jwt.options'] = [
  'language' => 'App\SimpleUserJWT\Languages\French', // This class contains messages constants, you can create your own with the same structure
  'controller' => 'App\SimpleUserJWT\UserController', // User controller, you can rewrite it
  'class' => 'App\Model\User', // If you want your own class, extends 'SimpleUser\JWT\User'
  'registrations' => [
    'enabled' => true,
    'confirm' => false // Send a mail to the user before enable it
  ],
  'invite' => [
    'enabled' => false // Allow user to send invitations
  ],
  'forget' => [
    'enabled' => false // Enable the 'forget password' function
  ],
  'tables' => [ // SQL tables
    'users' => 'users',
    'customfields' => 'user_custom_fields'
  ],
  'mailer' => [
    'enabled' => false,
    'from' => [
      'email' => 'do-not-reply@barez-vous.com',
      'name' => "Bar'ez-Vous!"
    ],
    // Email templates
    'templates' => [
      'register' => [
        'confirm' => 'confirm.twig',
        'welcome' => 'welcome.twig'
      ],
      'invite' => 'invite.twig',
      'forget' => 'forget.twig'
    ],
    // Routes name for email templates generation (optional if you don't want to use url in your email)
    'routes' => [
      'login' => 'user.jwt.login',
      'reset' => 'user.jwt.reset'
    ]
  ]
];

$app['security.role_hierarchy'] = [
  'ROLE_INVITED' => ['ROLE_USER'],
  'ROLE_REGISTERED' => ['ROLE_INVITED', 'ROLE_ALLOW_INVITE'],
  'ROLE_ADMIN' => ['ROLE_REGISTERED']
];

$app['security.firewalls'] = [
  'login' => [
    'pattern' => 'register|login|forget|reset',
    'anonymous' => true
  ],
  'secured' => [
    'pattern' => '^.*secured.*$|profil',
    'users' => $app->share(function($app) { return $app['user.manager']; }),
    'jwt' => [
            'use_forward' => true,
            'require_previous_session' => false,
            'stateless' => true
    ]
  ]
];
$app['security.jwt'] = [
  'secret_key' => 'A*fqs*émqj5e/+"[ddçéùxeezjç"ùç893"&fsxmé$^;g39&f)',
  'life_time' => 2592000,
  'algorithm' => ['HS256'],
  'options' => [
    'header_name' => 'X-Access-Token',
    'username_claim' => 'email' // Needed for silex-simpleuser-jwt
  ]
];

$app->register(new SecurityServiceProvider());
$app->register(new SecurityJWTServiceProvider());
$app->register(new UserProvider());
$app->mount('/', new UserProvider());

// Register JSON data decoder for JSON requests
$app->before(function (Request $request) {
	if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
		$data = json_decode($request->getContent(), true);
		$request->request->replace(is_array($data) ? $data : array());
	}
});

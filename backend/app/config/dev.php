<?php

// Doctrine (db)
$app['db.options'] = array(
	'driver'   => 'pdo_mysql',
	'charset'  => 'utf8mb4',
	'host'     => 'localhost',  // Mandatory for PHPUnit testing
	'port'     => '3306',
	'dbname'   => 'barezvous',
	'user'     => 'root',
	'password' => '',
);

// enable the debug mode
$app['debug'] = true;

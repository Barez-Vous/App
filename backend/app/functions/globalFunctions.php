<?php
namespace App\Functions;
use DateTime;

class GlobalFunctions {
  public function checkConfirmation($var, $var_confirmation) {
    if (($var == '') OR ($var_confirmation == '')) {
      return FALSE;
    }
    if ($var != $var_confirmation) {
      return FALSE;
    }
    return TRUE;
  }

  public function getDatetimeNow() {
    $datetime = new DateTime();
    return $datetime->format('Y\-m\-d\ h:i:s');
  }
}
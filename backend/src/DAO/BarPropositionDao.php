<?php

namespace App\DAO;

use Doctrine\DBAL\Connection;
use App\Model\BarProposition;

class BarPropositionDao
{
    private $db;

    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    protected function getDb()
    {
        return $this->db;
    }

    public function findAll()
    {
        $sql = "SELECT * FROM bar_horaires_happy_hours_proposition";
        $result = $this->getDb()->fetchAll($sql);

        $entities = array();
        foreach ( $result as $row ) {
            $id = $row['id'];
            $entities[$id] = $this->buildDomainObjectsWithTime($row);
        }

        return $entities;
    }

    public function find($id)
    {
        $sql = "SELECT * FROM bar_horaires_happy_hours_proposition WHERE id=?";
        $row = $this->getDb()->fetchAssoc($sql, array($id));

        if ($row) {
            return $this->buildDomainObjectsWithTime($row);
        } else {
            throw new \Exception("No bar matching id ".$id);
        }
    }

    public function save(BarProposition $barProposition)
    {
        $barData = array(
    		'name' => $barProposition->getName(),
    		'latitude' => $barProposition->getLatitude(),
    		'longitude' => $barProposition->getLongitude(),
    		'houseNumber' => $barProposition->getHouseNumber(),
    		'street' => $barProposition->getStreet(),
    		'postCode' => $barProposition->getPostcode(),
    		'city' => $barProposition->getCity(),
    		'country' => $barProposition->getCountry(),
    		'email' => $barProposition->getEmail(),
    		'phoneNumber' => $barProposition->getPhoneNumber(),
    		'facebookPage' => $barProposition->getFacebookPage(),
    		'websiteUrl' => $barProposition->getWebsiteUrl(),
            'date_posted' => $barProposition->getDatePosted(),
    		'id_user' => $barProposition->getIdUser(),
            'validated' => $barProposition->getValidated(),
            'date_validated' => $barProposition->getDateValidated(),
            'id_bar' => $barProposition->getIdBar()
        );
        // TODO CHECK
        if ($barProposition->getId()) {
            $this->getDb()->update('bar_proposition', $barData, array('id' => $barProposition->getId()));
        } else {
            $this->getDb()->insert('bar_proposition', $barData);
            $id = $this->getDb()->lastInsertId();
            $barProposition->setId($id);
        }
    }

    public function saveHoraires(BarProposition $bar, $id_user)
    {
        $this->getDb()->delete('bar_horaire_proposition', array('id_bar' => $bar->getId()));
        $day_array = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
        foreach ($bar->getOpeningTimes() as $key => $value) {
            $day_int = 0;
            foreach ($day_array as $day) {
                $day_int += 1;
                if ($value[$day] == true) {
                    $barData = array(
                        'id_bar' => $bar->getId(),
                        'day'=> $day_int,
                        'opening_time'=> $value['opening_time'],
                        'closing_time'=> $value['closing_time'],
                        'id_user'=> $id_user,
                        'date_posted'=> date('Y-m-d H:i:s')
                    );
                    $this->getDb()->insert('bar_horaire_proposition', $barData);
                }
            }
        }
    }

    public function saveHappyHours(BarProposition $bar, $id_user)
    {
        $this->getDb()->delete('bar_happy_hours_proposition', array('id_bar' => $bar->getId()));
        $day_array = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
        foreach ($bar->getHappyHours() as $key => $value) {
            $day_int = 0;
            foreach ($day_array as $day) {
                $day_int += 1;
                if ($value[$day] == true) {
                    $barData = array(
                        'id_bar' => $bar->getId(),
                        'day'=> $day_int,
                        'start_time'=> $value['start_time'],
                        'end_time'=> $value['end_time'],
                        'id_user'=> $id_user,
                        'date_posted'=> date('Y-m-d H:i:s')
                    );
                    $this->getDb()->insert('bar_happy_hours_proposition', $barData);
                }
            }
        }
    }

    public function delete($barProposition)
    {
        $this->getDb()->delete('bar_proposition', array('id' => $barProposition->getId()));
    }

    protected function buildDomainObjectsWithTime($row)
    {
        $bar = new BarProposition();
        $bar->setId($row['id']);
        $bar->setLatitude($row['latitude']);
        $bar->setLongitude($row['longitude']);
        $bar->setName($row['name']);
        $bar->setLatitude($row['latitude']);
        $bar->setLongitude($row['longitude']);
		$bar->setHouseNumber($row['houseNumber']);
		$bar->setStreet($row['street']);
		$bar->setPostcode($row['postCode']);
		$bar->setCity($row['city']);
		$bar->setCountry($row['country']);
		$bar->setEmail($row['email']);
		$bar->setPhoneNumber($row['phoneNumber']);
		$bar->setFacebookPage($row['facebookPage']);
		$bar->setWebsiteUrl($row['websiteUrl']);
		$bar->setDatePosted($row['date_posted']);
        $bar->setIdBar($row['id_bar']);
        $bar->setIdUser($row['id_user']);
        $bar->setValidated($row['validated']);
        $bar->setDateValidated($row['date_validated']);
        $day_array = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
        $openingTimes = array();
        $happyHours = array();
        foreach ($day_array as $day) {
            $openingKey = 'opening_' . $day;
            $harryHoursKey = 'happy_hours_' . $day;
            if ($row[$openingKey])
                $openingTimes[$day] = $row[$openingKey];
            if ($row[$harryHoursKey])
                $happyHours[$day] = $row[$harryHoursKey];
        }
        $bar->setOpeningTimes($openingTimes);
        $bar->setHappyHours($happyHours);

        return $bar;
    }

  	public function countAll()
  	{
		$sql = "SELECT COUNT(*) AS nbBar FROM bar";
		$row = $this->getDb()->fetchAssoc($sql);
		return $row['nbBar'];
    }

    public function validateHoraires(BarProposition $bar)
    {
        $this->getDb()->delete('bar_horaire', array('id_bar' => $bar->getIdBar()));
        $day_array = [
            'mon' => '1',
            'tue' => '2',
            'wed' => '3',
            'thu' => '4',
            'fri' => '5',
            'sat' => '6',
            'sun' => '7',
        ];
        $openingTimes = $bar->getOpeningTimes();
        foreach ($openingTimes as $day => $time) {
            if ($time) {
                $time = explode(' - ', $time);
                $barData = array(
                    'id_bar' => $bar->getIdBar(),
                    'day'=> $day_array[$day],
                    'opening_time'=> $time[0],
                    'closing_time'=> $time[1]
                );
                $this->getDb()->insert('bar_horaire', $barData);
            }
        }
    }

    public function validateHappyHours(BarProposition $bar)
    {
        $this->getDb()->delete('bar_happy_hours', array('id_bar' => $bar->getIdBar()));
        $day_array = [
            'mon' => '1',
            'tue' => '2',
            'wed' => '3',
            'thu' => '4',
            'fri' => '5',
            'sat' => '6',
            'sun' => '7',
        ];
        $happyHours = $bar->gethappyHours();
        foreach ($happyHours as $day => $time) {
            if ($time) {
                $time = explode(' - ', $time);
                $barData = array(
                    'id_bar' => $bar->getIdBar(),
                    'day'=> $day_array[$day],
                    'start_time'=> $time[0],
                    'end_time'=> $time[1]
                );
                $this->getDb()->insert('bar_happy_hours', $barData);
            }
        }
    }
}

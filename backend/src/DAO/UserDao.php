<?php

namespace App\DAO;

use Doctrine\DBAL\Connection;
use App\Model\User;

class UserDao
{
	private $db;

	public function __construct(Connection $db)
	{
		$this->db = $db;
	}

	protected function getDb()
	{
		return $this->db;
	}

	public function findAll()
	{
		$sql = "SELECT * FROM users";
		$result = $this->getDb()->fetchAll($sql);

		$entities = array();
		foreach ( $result as $row ) {
			$id = $row['id'];
			$entities[$id] = $this->buildDomainObjects($row);
		}

		return $entities;
	}

	public function find($id)
	{
		$sql = "SELECT * FROM users WHERE id=?";
		$row = $this->getDb()->fetchAssoc($sql, array($id));

		if ($row) {
			return $app['user.manager']->getUser($id);
		} else {
			throw new \Exception("No user matching id ".$id);
		}
	}

	public function save(User $user)
	{
		$userData = array(
			'username' => $user->getUsername(),
			'email' => $user->getEmail(),
			'password' => $user->getPassword(),
			'firstName' => $user->getFirstName(),
			'lastName' => $user->getLastName(),
			'dateCreated' => $user->getDateCreated()
		);

		if ($user->getId()) {
			$this->getDb()->update('users', $userData, array('id' => $user->getId()));
		} else {
			$this->getDb()->insert('users', $userData);
			$id = $this->getDb()->lastInsertId();
			$user->setId($id);
		}
	}

	public function delete($id)
	{
		$this->getDb()->delete('users', array('id' => $id));
	}

	protected function buildDomainObjects($row)
	{
		$user = new User($row['email']);
		$user->setId($row['id']);
		$user->setUsername($row['username']);
		$user->setEmail($row['email']);
		// $user->setFirstname($row['firstName']);
		// $user->setLastname($row['lastName']);
		// $user->setUseRealName($row['useRealName']);
		// $user->setPicture($row['picture']);
		return $user;
	}

	public function isUsernameAvailable($username) {
		$sql = "SELECT * FROM users WHERE username=?";
		$row = $this->getDb()->fetchAssoc($sql, array($username));

		if ($row)
			return FALSE;
		return TRUE;
	}

	public function isEmailAvailable($email) {
		$sql = "SELECT * FROM users WHERE email=?";
		$row = $this->getDb()->fetchAssoc($sql, array($email));

		if ($row)
			return FALSE;
		return TRUE;

	}

	public function countAll()
	{
		$sql = "SELECT COUNT(*) AS nbUser FROM users ";
		$row = $this->getDb()->fetchAssoc($sql);
		return $row['nbUser'];
	}

	public function countConnectWeek()
	{
		$sql = "SELECT COUNT(*) AS nbConnectWeek FROM users
						INNER JOIN user_custom_fields as ucf
						ON ucf.user_id = users.id
						WHERE (attribute = 'lastLogin')
						AND (STR_TO_DATE(ucf.value, '%Y-%m-%d') between date_sub(now(),INTERVAL 1 WEEK) and now())";
		$row = $this->getDb()->fetchAssoc($sql);
		return $row['nbConnectWeek'];
	}
}

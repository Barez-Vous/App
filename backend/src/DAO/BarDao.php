<?php

namespace App\DAO;

use Doctrine\DBAL\Connection;
use App\Model\Bar;

class BarDao
{
    private $db;

    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    protected function getDb()
    {
        return $this->db;
    }

    public function findAll()
    {
        $sql = "SELECT * FROM bar_horaires_happy_hours";
        $result = $this->getDb()->fetchAll($sql);

        $entities = array();
        foreach ( $result as $row ) {
            $id = $row['id'];
            $entities[$id] = $this->buildDomainObjectsWithTime($row);
        }

        return $entities;
    }

    public function find($id)
    {
        $sql = "SELECT * FROM bar_horaires_happy_hours WHERE id=?";
        $row = $this->getDb()->fetchAssoc($sql, array($id));

        if ($row) {
            return $this->buildDomainObjectsWithTime($row);
        } else {
            throw new \Exception("No bar matching id ".$id);
        }
    }

    public function findInRange($latitude, $longitude, $range)
    {
        $sql = "SELECT *,
            ( 6371 * acos( cos( radians( :lat ) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians( :long ) ) + sin(radians( :lat )) * sin(radians( latitude ))) ) As distance
            FROM bar_horaires_happy_hours
            HAVING distance < :range
            ORDER BY distance";
        $result = $this->getDb()->fetchAll($sql, array(
            ":lat" => $latitude,
            ":long" => $longitude,
            ":range" => $range
        ));

        $entities = array();
        foreach ( $result as $row ) {
            $id = $row['id'];
            $entities[$id] = $this->buildDomainObjectsWithTime($row);
        }

        return $entities;
    }

    public function save(Bar $bar)
    {
        $barData = array(
    		'name' => $bar->getName(),
    		'latitude' => $bar->getLatitude(),
    		'longitude' => $bar->getLongitude(),
    		'houseNumber' => $bar->getHouseNumber(),
    		'street' => $bar->getStreet(),
    		'postCode' => $bar->getPostcode(),
    		'city' => $bar->getCity(),
    		'country' => $bar->getCountry(),
    		'email' => $bar->getEmail(),
    		'phoneNumber' => $bar->getPhoneNumber(),
    		'facebookPage' => $bar->getFacebookPage(),
    		'websiteUrl' => $bar->getWebsiteUrl()
        );

        // TODO CHECK
        if ($bar->getId()) {
            $this->getDb()->update('bar', $barData, array('id' => $bar->getId()));
        } else {
            $this->getDb()->insert('bar', $barData);
            $id = $this->getDb()->lastInsertId();
            $bar->setId($id);
        }
    }

    public function saveHoraires(Bar $bar)
    {
        $this->getDb()->delete('bar_horaire', array('id_bar' => $bar->getId()));
        $day_array = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
        foreach ($bar->getOpeningTimes() as $key => $value) {
            $day_int = 0;
            foreach ($day_array as $day) {
                $day_int += 1;
                if ($value[$day] == true) {
                    $barData = array(
                        'id_bar' => $bar->getId(),
                        'day'=> $day_int,
                        'opening_time'=> $value['opening_time'],
                        'closing_time'=> $value['closing_time']
                    );
                    $this->getDb()->insert('bar_horaire', $barData);
                }
            }
        }
    }

    public function saveHappyHours(Bar $bar)
    {
        $this->getDb()->delete('bar_happy_hours', array('id_bar' => $bar->getId()));
        $day_array = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
        foreach ($bar->getHappyHours() as $key => $value) {
            $day_int = 0;
            foreach ($day_array as $day) {
                $day_int += 1;
                if ($value[$day] == true) {
                    $barData = array(
                        'id_bar' => $bar->getId(),
                        'day'=> $day_int,
                        'start_time'=> $value['start_time'],
                        'end_time'=> $value['end_time']
                    );
                    $this->getDb()->insert('bar_happy_hours', $barData);
                }
            }
        }
    }

    public function delete($id)
    {
        $barData = array(
            'deleted' => true
        );
        $this->getDb()->update('bar', $barData, array('id' => $id));
    }

    protected function buildDomainObjects($row)
    {
        $bar = new Bar();
        $bar->setId($row['id']);
        $bar->setName($row['name']);
        $bar->setLatitude($row['latitude']);
        $bar->setLongitude($row['longitude']);
		$bar->setHouseNumber($row['houseNumber']);
		$bar->setStreet($row['street']);
		$bar->setPostcode($row['postCode']);
		$bar->setCity($row['city']);
		$bar->setCountry($row['country']);
		$bar->setNote($row['note']);

        return $bar;
    }

    protected function buildDomainObjectsWithTime($row)
    {
        $bar = new Bar();
        $bar->setId($row['id']);
        $bar->setLatitude($row['latitude']);
        $bar->setLongitude($row['longitude']);
        $bar->setName($row['name']);
        $bar->setLatitude($row['latitude']);
        $bar->setLongitude($row['longitude']);
		$bar->setHouseNumber($row['houseNumber']);
		$bar->setStreet($row['street']);
		$bar->setPostcode($row['postCode']);
		$bar->setCity($row['city']);
		$bar->setCountry($row['country']);
		$bar->setEmail($row['email']);
		$bar->setPhoneNumber($row['phoneNumber']);
		$bar->setFacebookPage($row['facebookPage']);
		$bar->setWebsiteUrl($row['websiteUrl']);
		$bar->setNote($row['note']);
        $day_array = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
        $openingTimes = array();
        $happyHours = array();
        foreach ($day_array as $day) {
            $openingKey = 'opening_' . $day;
            $harryHoursKey = 'happy_hours_' . $day;
            if ($row[$openingKey])
                $openingTimes[$day] = $row[$openingKey];
            if ($row[$harryHoursKey])
                $happyHours[$day] = $row[$harryHoursKey];
        }
        $bar->setOpeningTimes($openingTimes);
        $bar->setHappyHours($happyHours);

        return $bar;
    }

  	public function countAll()
  	{
		$sql = "SELECT COUNT(*) AS nbBar FROM bar";
		$row = $this->getDb()->fetchAssoc($sql);
		return $row['nbBar'];
  	}
}

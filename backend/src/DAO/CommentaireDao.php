<?php

namespace App\DAO;

use Doctrine\DBAL\Connection;
use App\Model\Bar;
use App\Model\Commentaire;

class CommentaireDao
{
    private $db;

    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    protected function getDb()
    {
        return $this->db;
    }

    public function findAll()
    {
        $sql = "SELECT * FROM bar_commentaires";
        $result = $this->getDb()->fetchAll($sql);

        $entities = array();
        foreach ( $result as $row ) {
            $id = $row['id'];
            $entities[$id] = $this->buildDomainObjects($row);
        }

        return $entities;
    }

    public function find($id)
    {
        $sql = "SELECT * FROM bar_commentaires WHERE id=?";
        $row = $this->getDb()->fetchAssoc($sql, array($id));

        if ($row) {
            return $this->buildDomainObjects($row);
        } else {
            throw new \Exception("No bar matching id ".$id);
        }
    }

    protected function buildDomainObjects($row)
    {
        $commentaire = new Commentaire();
        $commentaire->setId($row['id']);
        $commentaire->setIdBar($row['id_bar']);
        $commentaire->setIdUser($row['id_user']);
        $commentaire->setNote($row['note']);
        $commentaire->setCommentaire($row['commentaire']);
        $commentaire->setDatePosted($row['date_posted']);
        $commentaire->setValidated($row['validated']);

        return $commentaire;
    }

    public function getBarCommentaires($id_bar)
    {
        $sql = "SELECT * FROM bar_commentaires WHERE id_bar=? AND validated = 1
                ORDER BY date_posted DESC";
        $result = $this->getDb()->fetchAll($sql, array($id_bar));

        $entities = array();
        foreach ($result as $row ) {
            $id = $row['id'];
            $entities[$id] = $this->buildDomainObjects($row);
        }
        return $entities;
    }

    public function save(Commentaire $commentaire)
    {
        $commentaireData = array(
          'id' => $commentaire->getId(),
          'id_bar' => $commentaire->getIdBar(),
          'id_user' => $commentaire->getIdUser(),
          'commentaire' => $commentaire->getCommentaire(),
          'note' => $commentaire->getNote(),
          'date_posted' => $commentaire->getDatePosted(),
          'validated' => $commentaire->getValidated()
        );
        // TODO CHECK
        if ($commentaire->getId()) {
            $this->getDb()->update('bar_commentaires', $commentaireData, array('id' => $commentaire->getId()));
        } else {
            $this->getDb()->insert('bar_commentaires', $commentaireData);
            $id = $this->getDb()->lastInsertId();
            $commentaire->setId($id);
        }
    }
}

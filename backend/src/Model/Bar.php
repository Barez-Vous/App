<?php

namespace App\Model;

class Bar
{
  /**
   * @var integer
   */
  private $id;

  /**
   * @var string
   */
  private $name;

  /**
   * @var string
   */
  private $latitude;

  /**
   * @var string
   */
  private $longitude;

  /**
   * @var string
   */
  private $houseNumber;

  /**
   * @var string
   */
  private $street;

  /**
   * @var string
   */
  private $postCode;

  /**
   * @var string
   */
  private $city;

  /**
   * @var string
   */
  private $country;

  /**
   * @var string
   */
  private $email;

  /**
   * @var string
   */
  private $phoneNumber;

  /**
   * @var string
   */
  private $facebookPage;

  /**
   * @var string
   */
  private $websiteUrl;

  /**
   * @var array
   */
  private $openingTimes;

  /**
   * @var array
   */
  private $happyHours;

  /**
   * @var integer
   */
  private $note;

	/**
	* Retourne la valeur de Id
	*
	* @return integer
	 */
	 public function getId()
	 {
	 return $this->id;
	}

	/**
	 * Défini la valeur de Id
	 *
	 * @param integer id
	 */
	public function setId($id)
	{
	    $this->id = $id;
	}

	/**
	* Retourne la valeur de Name
	*
	* @return string
	 */
	 public function getName()
	 {
	 return $this->name;
	}

	/**
	 * Défini la valeur de Name
	 *
	 * @param string name
	 */
	public function setName($name)
	{
	    $this->name = $name;
	}

	/**
	* Retourne la valeur de Latitude
	*
	* @return string
	 */
	 public function getLatitude()
	 {
	 return $this->latitude;
	}

	/**
	 * Défini la valeur de Latitude
	 *
	 * @param string latitude
	 */
	public function setLatitude($latitude)
	{
	    $this->latitude = $latitude;
	}

	/**
	* Retourne la valeur de Longitude
	*
	* @return string
	 */
	 public function getLongitude()
	 {
	 return $this->longitude;
	}

	/**
	 * Défini la valeur de Longitude
	 *
	 * @param string longitude
	 */
	public function setLongitude($longitude)
	{
	    $this->longitude = $longitude;
	}

	/**
	* Retourne la valeur de House Number
	*
	* @return string
	 */
	 public function getHouseNumber()
	 {
	 return $this->houseNumber;
	}

	/**
	 * Défini la valeur de House Number
	 *
	 * @param string houseNumber
	 */
	public function setHouseNumber($houseNumber)
	{
	    $this->houseNumber = $houseNumber;
	}

	/**
	* Retourne la valeur de Street
	*
	* @return string
	 */
	 public function getStreet()
	 {
	 return $this->street;
	}

	/**
	 * Défini la valeur de Street
	 *
	 * @param string street
	 */
	public function setStreet($street)
	{
	    $this->street = $street;
	}

	/**
	* Retourne la valeur de Post Code
	*
	* @return string
	 */
	 public function getPostCode()
	 {
	 return $this->postCode;
	}

	/**
	 * Défini la valeur de Post Code
	 *
	 * @param string postCode
	 */
	public function setPostCode($postCode)
	{
	    $this->postCode = $postCode;
	}

	/**
	* Retourne la valeur de City
	*
	* @return string
	 */
	 public function getCity()
	 {
	 return $this->city;
	}

	/**
	 * Défini la valeur de City
	 *
	 * @param string city
	 */
	public function setCity($city)
	{
	    $this->city = $city;
	}

	/**
	* Retourne la valeur de Country
	*
	* @return string
	 */
	 public function getCountry()
	 {
	 return $this->country;
	}

	/**
	 * Défini la valeur de Country
	 *
	 * @param string country
	 */
	public function setCountry($country)
	{
	    $this->country = $country;
	}

	/**
	* Retourne la valeur de Email
	*
	* @return string
	 */
	 public function getEmail()
	 {
	 return $this->email;
	}

	/**
	 * Défini la valeur de Email
	 *
	 * @param string email
	 */
	public function setEmail($email)
	{
	    $this->email = $email;
	}

	/**
	* Retourne la valeur de Phone Number
	*
	* @return string
	 */
	 public function getPhoneNumber()
	 {
	 return $this->phoneNumber;
	}

	/**
	 * Défini la valeur de Phone Number
	 *
	 * @param string phoneNumber
	 */
	public function setPhoneNumber($phoneNumber)
	{
	    $this->phoneNumber = $phoneNumber;
	}

	/**
	* Retourne la valeur de Facebook Page
	*
	* @return string
	 */
	 public function getFacebookPage()
	 {
	 return $this->facebookPage;
	}

	/**
	 * Défini la valeur de Facebook Page
	 *
	 * @param string facebookPage
	 */
	public function setFacebookPage($facebookPage)
	{
	    $this->facebookPage = $facebookPage;
	}

	/**
	* Retourne la valeur de Website Url
	*
	* @return string
	 */
	 public function getWebsiteUrl()
	 {
	 return $this->websiteUrl;
	}

	/**
	 * Défini la valeur de Website Url
	 *
	 * @param string websiteUrl
	 */
	public function setWebsiteUrl($websiteUrl)
	{
	    $this->websiteUrl = $websiteUrl;
	}

	/**
	* Retourne la valeur de Opening Times
	*
	* @return array
	 */
	 public function getOpeningTimes()
	 {
	 return $this->openingTimes;
	}

	/**
	 * Défini la valeur de Opening Times
	 *
	 * @param array openingTimes
	 */
	public function setOpeningTimes(array $openingTimes)
	{
	    $this->openingTimes = $openingTimes;
	}

	/**
	* Retourne la valeur de Happy Hours
	*
	* @return array
	 */
	 public function getHappyHours()
	 {
	 return $this->happyHours;
	}

	/**
	 * Défini la valeur de Happy Hours
	 *
	 * @param array happyHours
	 */
	public function setHappyHours(array $happyHours)
	{
	    $this->happyHours = $happyHours;
	}

	/**
	* Retourne la valeur de Note
	*
	* @return integer
	 */
	 public function getNote()
	 {
	 return $this->note;
	}

	/**
	 * Défini la valeur de Note
	 *
	 * @param integer note
	 */
	public function setNote($note)
	{
	    $this->note = $note;
	}
}

<?php

namespace App\Model;

use App\SimpleUserJWT\User as SimpleUser;
use Datetime;
class User extends SimpleUser
{
	/**
	 * @return string
	 */
	public function getFirstName() {
		return $this->getCustomField('firstName');
	}

	/**
	 * @param string $firstName
	 */
	public function setFirstName( $firstName ) {
		$this->setCustomField('firstName', $firstName);
	}

	/**
	 * @return string
	 */
	public function getLastName() {
		return $this->getCustomField('lastName');
	}

	/**
	 * @param string $lastName
	 */
	public function setLastName( $lastName ) {
		$this->setCustomField('lastName', $lastName);
	}

	/**
	 * @return datetime
	 */
	public function getDateCreated() {
		return $this->getCustomField('dateCreated');
	}

	/**
	 * @param datetime $dateCreated
	 */
	public function setDateCreated() {
		$dateCreated = date('Y-m-d H:i:s');
		$this->setCustomField('dateCreated', $dateCreated);
	}

	/**
	 * @return date
	 */
	public function getLastLogin() {
		return $this->getCustomField('lastLogin');
	}

	/**
	 * @param date $lastLogin
	 */
	public function setLastLogin() {
		$lastLogin = date('Y-m-d H:i:s');
		$this->setCustomField('lastLogin', $lastLogin);
	}

	/**
	 * @return date
	 */
	public function getUseRealName() {
		return $this->getCustomField('useRealName');
	}

	/**
	 * @param date $useRealName
	 */
	public function setUseRealName( $useRealName ) {
		$this->setCustomField('useRealName', $useRealName);
	}

	/**
	 * @return string
	 */
	public function getPicture() {
		return $this->getCustomField('picture');
	}

	/**
	 * @param string $picture
	 */
	public function setPicture( $picture ) {
		$this->setCustomField('picture', $picture);
	}


}

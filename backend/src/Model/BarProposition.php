<?php

namespace App\Model;

class BarProposition
{
	/**
	 * @var integer
	 */
	private $id;

	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var string
	 */
	private $latitude;

	/**
	 * @var string
	 */
	private $longitude;

	/**
	 * @var string
	 */
	private $houseNumber;

	/**
	 * @var string
	 */
	private $street;

	/**
	 * @var string
	 */
	private $postCode;

	/**
	 * @var string
	 */
	private $city;

	/**
	 * @var string
	 */
	private $country;

	/**
	 * @var string
	 */
	private $email;

	/**
	 * @var string
	 */
	private $phoneNumber;

	/**
	 * @var string
	 */
	private $facebookPage;

	/**
	 * @var string
	 */
	private $websiteUrl;

	/**
	 * @var array
	 */
	private $openingTimes;

	/**
	 * @var array
	 */
	private $happyHours;

	/**
	 * @var integer
	 */
	private $id_user;
	
	/**
	 * @var string
	 */
	private $date_posted;

	/**
	 * @var boolean
	 */
	private $validated;

	/**
	 * @var string
	 */
	private $date_validated;

	/**
	 * @var integer
	 */
	private $id_bar;

	
	/**
	* Retourne la valeur de Id
	*
	* @return integer
	 */
	 public function getId()
	 {
	 return $this->id;
	}

	/**
	 * Défini la valeur de Id
	 *
	 * @param integer id
	 */
	public function setId($id)
	{
	    $this->id = $id;
	}

	/**
	* Retourne la valeur de Name
	*
	* @return string
	 */
	 public function getName()
	 {
	 return $this->name;
	}

	/**
	 * Défini la valeur de Name
	 *
	 * @param string name
	 */
	public function setName($name)
	{
	    $this->name = $name;
	}

	/**
	* Retourne la valeur de Latitude
	*
	* @return string
	 */
	 public function getLatitude()
	 {
	 return $this->latitude;
	}

	/**
	 * Défini la valeur de Latitude
	 *
	 * @param string latitude
	 */
	public function setLatitude($latitude)
	{
	    $this->latitude = $latitude;
	}

	/**
	* Retourne la valeur de Longitude
	*
	* @return string
	 */
	 public function getLongitude()
	 {
	 return $this->longitude;
	}

	/**
	 * Défini la valeur de Longitude
	 *
	 * @param string longitude
	 */
	public function setLongitude($longitude)
	{
	    $this->longitude = $longitude;
	}

	/**
	* Retourne la valeur de House Number
	*
	* @return string
	 */
	 public function getHouseNumber()
	 {
	 return $this->houseNumber;
	}

	/**
	 * Défini la valeur de House Number
	 *
	 * @param string houseNumber
	 */
	public function setHouseNumber($houseNumber)
	{
	    $this->houseNumber = $houseNumber;
	}

	/**
	* Retourne la valeur de Street
	*
	* @return string
	 */
	 public function getStreet()
	 {
	 return $this->street;
	}

	/**
	 * Défini la valeur de Street
	 *
	 * @param string street
	 */
	public function setStreet($street)
	{
	    $this->street = $street;
	}

	/**
	* Retourne la valeur de Post Code
	*
	* @return string
	 */
	 public function getPostCode()
	 {
	 return $this->postCode;
	}

	/**
	 * Défini la valeur de Post Code
	 *
	 * @param string postCode
	 */
	public function setPostCode($postCode)
	{
	    $this->postCode = $postCode;
	}

	/**
	* Retourne la valeur de City
	*
	* @return string
	 */
	 public function getCity()
	 {
	 return $this->city;
	}

	/**
	 * Défini la valeur de City
	 *
	 * @param string city
	 */
	public function setCity($city)
	{
	    $this->city = $city;
	}

	/**
	* Retourne la valeur de Country
	*
	* @return string
	 */
	 public function getCountry()
	 {
	 return $this->country;
	}

	/**
	 * Défini la valeur de Country
	 *
	 * @param string country
	 */
	public function setCountry($country)
	{
	    $this->country = $country;
	}

	/**
	* Retourne la valeur de Email
	*
	* @return string
	 */
	 public function getEmail()
	 {
	 return $this->email;
	}

	/**
	 * Défini la valeur de Email
	 *
	 * @param string email
	 */
	public function setEmail($email)
	{
	    $this->email = $email;
	}

	/**
	* Retourne la valeur de Phone Number
	*
	* @return string
	 */
	 public function getPhoneNumber()
	 {
	 return $this->phoneNumber;
	}

	/**
	 * Défini la valeur de Phone Number
	 *
	 * @param string phoneNumber
	 */
	public function setPhoneNumber($phoneNumber)
	{
	    $this->phoneNumber = $phoneNumber;
	}

	/**
	* Retourne la valeur de Facebook Page
	*
	* @return string
	 */
	 public function getFacebookPage()
	 {
	 return $this->facebookPage;
	}

	/**
	 * Défini la valeur de Facebook Page
	 *
	 * @param string facebookPage
	 */
	public function setFacebookPage($facebookPage)
	{
	    $this->facebookPage = $facebookPage;
	}

	/**
	* Retourne la valeur de Website Url
	*
	* @return string
	 */
	 public function getWebsiteUrl()
	 {
	 return $this->websiteUrl;
	}

	/**
	 * Défini la valeur de Website Url
	 *
	 * @param string websiteUrl
	 */
	public function setWebsiteUrl($websiteUrl)
	{
	    $this->websiteUrl = $websiteUrl;
	}

	/**
	* Retourne la valeur de Opening Times
	*
	* @return array
	 */
	 public function getOpeningTimes()
	 {
	 return $this->openingTimes;
	}

	/**
	 * Défini la valeur de Opening Times
	 *
	 * @param array openingTimes
	 */
	public function setOpeningTimes(array $openingTimes)
	{
	    $this->openingTimes = $openingTimes;
	}

	/**
	* Retourne la valeur de Happy Hours
	*
	* @return array
	 */
	 public function getHappyHours()
	 {
	 return $this->happyHours;
	}

	/**
	 * Défini la valeur de Happy Hours
	 *
	 * @param array happyHours
	 */
	public function setHappyHours(array $happyHours)
	{
	    $this->happyHours = $happyHours;
	}

	/**
	 * Retourne la valeur de Id Bar
	*
	* @return integer
	*/
	public function getIdBar()
	{
		return $this->id_bar;
	}

	/**
	 * Défini la valeur de Id Bar
	*
	* @param integer id_bar
	*/
	public function setIdBar($id_bar)
	{
		$this->id_bar = $id_bar;
	}

	/**
	 * Retourne la valeur de Id User
	 *
	 * @return integer
	 */
	public function getIdUser()
	{
		return $this->id_user;
	}

	/**
	 * Défini la valeur de Id User
	*
	* @param integer id_user
	*/
	public function setIdUser($id_user)
	{
		$this->id_user = $id_user;
	}

	/**
	 * Retourne la valeur de Date Posted
	 *
	 * @return string
	 */
	public function getDatePosted()
	{
		return $this->date_posted;
	}

	/**
	 * Défini la valeur de Date Posted
	*
	* @param string date_posted
	*/
	public function setDatePosted()
	{
		$date_posted = date('Y-m-d H:i:s');
		$this->date_posted = $date_posted;
	}

	/**
	 * Retourne la valeur de Validated
	 *
	 * @return boolean
	 */
	public function getValidated()
	{
		return $this->validated;
	}

	/**
	 * Défini la valeur de Validated
	*
	* @param boolean date_posted
	*/
	public function setValidated($validated)
	{
		$this->validated = (bool) $validated;
	}

	/**
	 * Retourne la valeur de Date Validated
	 *
	 * @return string
	 */
	public function getDateValidated()
	{
		return $this->date_validated;
	}

	/**
	 * Défini la valeur de Date Validated
	*
	* @param string date_validated
	*/
	public function setDateValidated()
	{
		$date_validated = date('Y-m-d H:i:s');
		$this->date_validated = $date_validated;
	}
}

<?php

namespace App\Model;

class Commentaire
{
  /**
   * @var integer
   */
  private $id;

  /**
   * @var integer
   */
  private $id_bar;

  /**
   * @var integer
   */
  private $id_user;

  /**
   * @var string
   */
  private $commentaire;

  /**
   * @var string
   */
  private $note;

  /**
   * @var string
   */
  private $date_posted;

  /**
   * @var boolean
   */
  private $validated;

  /**
  * Retourne la valeur de Id
  *
  * @return integer
   */
   public function getId()
   {
     return $this->id;
   }

  /**
   * Défini la valeur de Id
   *
   * @param integer id
   */
  public function setId($id)
  {
      $this->id = $id;
  }

  /**
  * Retourne la valeur de Id Bar
  *
  * @return integer
   */
   public function getIdBar()
   {
   return $this->id_bar;
  }

  /**
   * Défini la valeur de Id Bar
   *
   * @param integer id_bar
   */
  public function setIdBar($id_bar)
  {
      $this->id_bar = $id_bar;
  }

  /**
  * Retourne la valeur de Id User
  *
  * @return integer
   */
   public function getIdUser()
   {
   return $this->id_user;
  }

  /**
   * Défini la valeur de Id User
   *
   * @param integer id_user
   */
  public function setIdUser($id_user)
  {
      $this->id_user = $id_user;
  }

  /**
  * Retourne la valeur de Commentaire
  *
  * @return string
   */
   public function getCommentaire()
   {
   return $this->commentaire;
  }

  /**
   * Défini la valeur de Note
   *
   * @param integer note
   */
  public function setCommentaire($commentaire)
  {
      $this->commentaire = $commentaire;
  }

  /**
  * Retourne la valeur de Note
  *
  * @return integer
   */
   public function getNote()
   {
   return $this->note;
  }

  /**
   * Défini la valeur de Note
   *
   * @param integer note
   */
  public function setNote($note)
  {
      $this->note = $note;
  }

  /**
  * Retourne la valeur de Date Posted
  *
  * @return string
   */
   public function getDatePosted()
   {
   return $this->date_posted;
  }

  /**
   * Défini la valeur de Date Posted
   *
   * @param string date_posted
   */
  public function setDatePosted(string $date_posted)
  {
      $this->date_posted = $date_posted;
  }

  /**
  * Retourne la valeur de Validated
  *
  * @return boolean
   */
   public function getValidated()
   {
   return $this->validated;
  }

  /**
   * Défini la valeur de Validated
   *
   * @param boolean date_posted
   */
  public function setValidated($validated)
  {
      $this->validated = (bool) $validated;
  }
}

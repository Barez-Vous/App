<?php

  namespace App\SimpleUserJWT\Languages;

  class English {
    /* Messages */
    const REGISTRATIONS_DISABLED = 'Registrations are disabled';
    const REGISTER_SUCCESS = 'Account created';

    const INVITATIONS_DISABLED = 'Invitations are disabled';
    const INVITATIONS_FORBIDDEN = "You're not allowed to send invitations";
    const INVITATIONS_SUCCESS = 'The user should receive an email soon with a confirmation of the invitation';

    const FORGET_DISABLED = 'Forget function are disabled';
    const FORGET_SUCCESS = 'Email to reset the password sent';

    const RESET_DISABLED = 'Reset function are disabled';
    const RESET_SUCCESS = 'Password reseted';

    /* Errors */
    const EMAIL_MISSING = 'Email missing';
    const EMAIL_OR_PASSWORD_MISSING = 'Email or password missing';
    const NAME_OR_USERNAME_MISSING = "Username or first name and last name missing";
    const EMAIL_CONFIRMATION_INCORRECT = 'The email confirmation is incorrect';
    const PASSWORD_CONFIRMATION_INCORRECT = 'The password confirmation is incorrect';
    const EMAIL_INVALID = 'Email not valid';
    const EMAIL_USED = 'Email already in use';
    const EMAIL_UNKNOWN = 'Email not registered';
    const PASSWORD_INVALID = 'Invalid password';
    const ACCOUNT_DISABLED = 'Account disabled';
    const TOKEN_INVALID = 'Token invalid';
    const TOKEN_EXPIRED = 'Token expired';
    const UNAUTHORIZED = "You're not allowed to do this action";
    const USERNAME_USED = 'Username already in use';
  }

?>

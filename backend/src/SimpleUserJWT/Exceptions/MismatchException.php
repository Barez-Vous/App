<?php

  namespace App\SimpleUserJWT\Exceptions;

  use Exception;

  class MismatchException extends Exception{}

?>

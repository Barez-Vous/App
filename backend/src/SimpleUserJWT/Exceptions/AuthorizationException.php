<?php

  namespace App\SimpleUserJWT\Exceptions;

  use Exception;

  class AuthorizationException extends Exception{}

?>

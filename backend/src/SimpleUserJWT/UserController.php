<?php

  namespace App\SimpleUserJWT;

  use Silex\Application;
  use Symfony\Component\HttpFoundation\Request;

  use Exception;
  use InvalidArgumentException;
  use App\SimpleUserJWT\Exceptions\AuthorizationException;
  use App\SimpleUserJWT\Exceptions\UnknownException;
  use App\SimpleUserJWT\Exceptions\DisabledException;
  use App\SimpleUserJWT\Exceptions\MismatchException;
  use App\SimpleUserJWT\Exceptions\ConfigException;
  use App\SimpleUserJWT\Exceptions\ExpiredException;
  use App\SimpleUserJWT\Exceptions\UsedException;

  class UserController{

    /**
     * Register a user (email & password only)
     * @method register
     * @param  string         $email    Email of the future user
     * @param  string         $password Password of the future user
     * @return json|Exception
     */
    public function register(Application $app, Request $request){
      $username = $request->request->get('username');
      $firstName = $request->request->get('firstName');
      $lastName = $request->request->get('lastName');
      $email = $request->request->get('email');
      $email_confirmation = $request->request->get('email_confirmation');
      $password = $request->request->get('password');
      $password_confirmation = $request->request->get('password_confirmation');

      if(!$app['user.jwt.options']['registrations']['enabled']){
        return $app->json($app['user.jwt.options']['language']::REGISTRATIONS_DISABLED, 400);
      }

      if(!$email OR !$password OR !$email_confirmation OR !$password_confirmation){
        return $app->json($app['user.jwt.options']['language']::EMAIL_OR_PASSWORD_MISSING, 400);
      }

      if(!$username AND !($firstName AND $lastName)){
        return $app->json($app['user.jwt.options']['language']::NAME_OR_USERNAME_MISSING, 400);
      }

      if($email != $email_confirmation){
        return $app->json($app['user.jwt.options']['language']::EMAIL_CONFIRMATION_INCORRECT, 400);
      }

      if($password != $password_confirmation){
        return $app->json($app['user.jwt.options']['language']::PASSWORD_CONFIRMATION_INCORRECT, 400);
      }

      if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
        return $app->json($app['user.jwt.options']['language']::EMAIL_INVALID, 400);
      }

      if($app['user.manager']->findOneBy(['email' => $email])){
        return $app->json($app['user.jwt.options']['language']::EMAIL_USED, 400);
      }

      $user = $app['user.manager']->createUser($email, $password);
      $user->setDateCreated();
      $username ? $user->setUsername($username) : null;
      $firstName ? $user->setFirstName($firstName) : null;
      $lastName ? $user->setLastName($lastName) : null;
      if ($firstName AND $lastName) {
        $user->setUseRealName(true);
      }
      $user->addRole('ROLE_REGISTERED');

      $answer = ['message' => $app['user.jwt.options']['language']::REGISTER_SUCCESS];

/*      if($app['user.jwt.options']['registrations']['confirm']){
        $user->setEnabled(false);
        $user->setTimePasswordResetRequested(time());
        $user->setConfirmationToken($app['user.tokenGenerator']->generateToken());
        $app['user.jwt.mailer']->send(
          $app['user.jwt.options']['mailer']['templates']['register']['confirm'],
          $app['user.jwt.options']['mailer']['routes']['reset'],
          $user,
          ['token' => $user->getConfirmationToken()]
        );
      } else{
        $answer['token'] = $app['security.jwt.encoder']->encode($user->serialize());
        if($app['user.jwt.options']['mailer']['templates']['register']['welcome']){
          $app['user.jwt.mailer']->send(
            $app['user.jwt.options']['mailer']['templates']['register']['welcome'],
            $app['user.jwt.options']['mailer']['routes']['login'],
            $user
          );
        }
      }*/

      $app['user.manager']->insert($user);

      return $app->json($answer);
    }

    /**
     * Authenticate a user by his email & password
     * @method login
     * @param  string         $email    Email of the user
     * @param  string         $password Clear password of the user
     * @return json|Exception
     */
    public function login(Application $app, Request $request){
      $email = $request->request->get('email');
      $password = $request->request->get('password');

      if(!$email OR !$password){
        return $app->json($app['user.jwt.options']['language']::EMAIL_OR_PASSWORD_MISSING, 400);
      }

      if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
        return $app->json($app['user.jwt.options']['language']::EMAIL_INVALID, 400);
      }

      $user = $app['user.manager']->findOneBy(['email' => $email]);

      if(!$user){
        return $app->json($app['user.jwt.options']['language']::EMAIL_UNKNOWN, 400);
      }

      if(!$app['security.encoder.digest']->isPasswordValid($user->getPassword(), $password, $user->getSalt())){
        return $app->json($app['user.jwt.options']['language']::PASSWORD_INVALID, 400);
      }

      if(!$user->isEnabled()){
        return $app->json($app['user.jwt.options']['language']::ACCOUNT_DISABLED, 400);
      }

      $user->setLastLogin();
      $app['user.manager']->update($user);
      return $app->json(['token' => $app['security.jwt.encoder']->encode($user->serialize()),
                         'roles' => $user->getRoles(),
                         'email' => $user->getEmail(),
                         'username' => $user->getUsername(),
                         'firstName' => $user->getFirstName(),
                         'lastName' => $user->getLastName(),
                         'picture' => $user->getPicture(),
                         'useRealName' => $user->getUseRealName(),
                         'id' => $user->getId()]);
    }

    /**
     * Create a user with a temp password and send him an email
     * @method invite
     * @param  string         $email            Email of the invited user
     * @return json|Exception
     */
    public function invite(Application $app, Request $request){
      $email = $request->request->get('email');

      if(!$app['user.jwt.options']['invite']['enabled']){
        throw new Exception($app['user.jwt.options']['language']::INVITATIONS_DISABLED);
      }

      if(!$app['security.authorization_checker']->isGranted('ROLE_ALLOW_INVITE')){
        throw new AuthorizationException($app['user.jwt.options']['language']::INVITATIONS_FORBIDDEN);
      }

      if(!$email){
        throw new InvalidArgumentException($app['user.jwt.options']['language']::EMAIL_MISSING);
      }

      if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
        throw new InvalidArgumentException($app['user.jwt.options']['language']::EMAIL_INVALID);
      }

      if($app['user.manager']->findOneBy(['email' => $email])){
        throw new UsedException($app['user.jwt.options']['language']::EMAIL_USED);
      }

      $user = $app['user.manager']->createUser($email, $app['user.tokenGenerator']->generateToken());
      $user->setCustomField('invited_by', $app['security']->getToken()->getUser()->getId());
      $user->addRole('ROLE_INVITED');

      $user->setEnabled(false);
      $user->setTimePasswordResetRequested(time());
      $user->setConfirmationToken($app['user.tokenGenerator']->generateToken());

      $app['user.manager']->insert($user);

      $app['user.jwt.mailer']->send(
        $app['user.jwt.options']['mailer']['templates']['invite'],
        $app['user.jwt.options']['mailer']['routes']['reset'],
        $user,
        ['token' => $user->getConfirmationToken()]
      );

      $answer = ['message' => $app['user.jwt.options']['language']::INVITATIONS_SUCCESS];

      if(isset($app['dev']) && $app['dev']){
        $answer['token'] = $user->getConfirmationToken();
      }

      return $app->json($answer);
    }

    /**
     * Return invited users by the logged user
     * @method friends
     * @return json|Exception
     */
    public function friends(Application $app, Request $request){
      $user = $app['security']->getToken()->getUser();

      $friends = $app['user.manager']->findby(['customFields' => ['invited_by' => $user->getId()]]);

      return $app->json(['friends' => $friends]);
    }

    /**
     * Set a reset password token and send an email to the user
     * @method forget
     * @param  string         $email Email of the user who forget his password
     * @return json|Exception
     */
    public function forget(Application $app, Request $request){
      $email = $request->request->get('email');

      if(!$app['user.jwt.options']['forget']['enabled']){
        throw new Exception($app['user.jwt.options']['language']::FORGET_DISABLED);
      }

      if(!$email){
        throw new InvalidArgumentException($app['user.jwt.options']['language']::EMAIL_MISSING);
      }

      if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
        throw new InvalidArgumentException($app['user.jwt.options']['language']::EMAIL_INVALID);
      }

      $user = $app['user.manager']->findOneBy(['email' => $email]);

      if(!$user){
        throw new UnknownException($app['user.jwt.options']['language']::EMAIL_UNKNOWN);
      }

      $user->setTimePasswordResetRequested(time());
      $user->setConfirmationToken($app['user.tokenGenerator']->generateToken());
      $app['user.manager']->update($user);

      $app['user.jwt.mailer']->send(
        $app['user.jwt.options']['mailer']['templates']['forget'],
        $app['user.jwt.options']['mailer']['routes']['reset'],
        $user,
        ['token' => $user->getConfirmationToken()]
      );

      $answer = ['message' => $app['user.jwt.options']['language']::FORGET_SUCCESS];

      if(isset($app['dev']) && $app['dev']){
        $answer['token'] = $user->getConfirmationToken();
      }

      return $app->json($answer);
    }

    /**
     * Reset a password for a given reset password token linked to an user
     * @method invite
     * @param  string         $token    Reset password token
     * @param  string         $password New password
     * @return json|Exception
     */
    public function reset(Application $app, Request $request, $token){
      $password = $request->request->get('password');

      if(!$app['user.jwt.options']['forget']['enabled']){
        throw new Exception($app['user.jwt.options']['language']::RESET_DISABLED);
      }

      $user = $app['user.manager']->findOneBy(['confirmationToken' => $token]);

      if(!$user){
        throw new InvalidArgumentException($app['user.jwt.options']['language']::TOKEN_INVALID);
      }

      if($user->isPasswordResetRequestExpired($app['user.options']['passwordReset']['tokenTTL'])){
        throw new ExpiredException($app['user.jwt.options']['language']::TOKEN_EXPIRED);
      }

      $user->setEnabled(true);
      $user->setTimePasswordResetRequested(null);
      $user->setConfirmationToken(null);
      $app['user.manager']->setUserPassword($user, $password);
      $app['user.manager']->update($user);

      return $app->json(['message' => $app['user.jwt.options']['language']::RESET_SUCCESS, 'token' => $app['security.jwt.encoder']->encode($user->serialize())]);
    }

    /**
     * Update the profil of the current logged user
     * @method update
     * @param  int            $id (optional)           ID of the user we want to update
     * @param  string         $email (optional)        New email
     * @param  string         $password (optional)     New password
     * @param  string         $name (optional)         New name
     * @param  string         $username (optional)     New username
     * @param  array          $customFields (optional) New custom fields
     * @return json|Exception
     */
    public function update(Application $app, Request $request, $id){
      $email = $request->request->get('email');
      $email_confirmation = $request->request->get('email_confirmation');
      $current_password = $request->request->get('current_password');
      $password = $request->request->get('password');
      $password_confirmation = $request->request->get('password_confirmation');
      $name = $request->request->get('name');
      $username = $request->request->get('username');
      $customFields = $request->request->get('customFields');
      if($id == '-1'){
        $user = $app['security']->getToken()->getUser();
        if(!$app['security.encoder.digest']->isPasswordValid($user->getPassword(), $current_password, $user->getSalt())){
          return $app->json($app['user.jwt.options']['language']::PASSWORD_INVALID, 400);
        }
      } else if($app['security.authorization_checker']->isGranted('ROLE_ADMIN')){
        $roles = $request->request->get('roles');
        $user = $app['user.manager']->findOneBy(['id' => $id]);
        if ($roles){
          $user->setRoles($roles);
        }
      } else{
        return $app->json($app['user.jwt.options']['language']::UNAUTHORIZED, 400);
      }

      if($email != $user->getEmail()) {
        if($email && filter_var($email, FILTER_VALIDATE_EMAIL)){
          if ($id == '-1') {
            if ($email != $email_confirmation) {
              return $app->json($app['user.jwt.options']['language']::EMAIL_CONFIRMATION_INCORRECT, 400);
            }
          }
          if($app['user.manager']->findOneBy(['email' => $email])){
            return $app->json($app['user.jwt.options']['language']::EMAIL_USED, 400);
          }
          $user->setEmail($email);
        }
      }
      if ($password) {
        if($password == $password_confirmation){
          $app['user.manager']->setUserPassword($user, $password);
        } else {
          return $app->json($app['user.jwt.options']['language']::PASSWORD_CONFIRMATION_INCORRECT, 400);
        }
      }

      if($username != $user->getUsername()){
        if($app['user.manager']->findOneBy(['username' => $username])){
          return $app->json($app['user.jwt.options']['language']::USERNAME_USED, 400);
        }
        $user->setUsername($username);
      }

      if($name){
        $user->setName($name);
      }

      if($customFields){
        $user->setCustomFields($customFields);
      }

      $app['user.manager']->update($user);

      return $app->json(['token' => $app['security.jwt.encoder']->encode($user->serialize())]);
    }

  }

?>

<?php

namespace App\Routes;

use App\SimpleUserJWT\Exceptions\AuthorizationException;
use Symfony\Component\HttpFoundation\Request;
use App\Model\Bar;

// Get all users
$app->get('/api/bars', function () use ($app) {
	$bars = $app['dao.bar']->findAll();

	$responseData = array();
	foreach ($bars as $bar) {
		$openingTimes = $bar->getOpeningTimes();
		$openingTimesArray = array();
		foreach ($openingTimes as $day => $time) {
			if ($time)
				$time = explode(' - ', $time);
			else
				$time = ['00:00:00', '00:00:00'];
			array_push($openingTimesArray,
				array(
					'day' => $day,
					'opening_time' => $time[0],
					'closing_time' => $time[1]
				)
			);
		}
		$happyHours = $bar->gethappyHours();
		$happyHoursArray = array();
		foreach ($happyHours as $day => $time) {
			if ($time)
				$time = explode(' - ', $time);
			else
				$time = ['00:00:00', '00:00:00'];
			array_push($happyHoursArray,
				array(
					'day' => $day,
					'start_time' => $time[0],
					'end_time' => $time[1]
				)
			);
		}
		$responseData[] = array(
			'id' => $bar->getId(),
			'name' => $bar->getName(),
			'latitude' => $bar->getLatitude(),
			'longitude' => $bar->getLongitude(),
			'address' => $bar->getHouseNumber() . " " . $bar->getStreet(),
			'houseNumber' => $bar->getHouseNumber(),
			'street' => $bar->getStreet(),
			'postCode' => $bar->getPostcode(),
			'city' => $bar->getCity(),
			'country' => $bar->getCountry(),
			'email' => $bar->getEmail(),
			'phoneNumber' => $bar->getPhoneNumber(),
			'facebookPage' => $bar->getFacebookPage(),
			'websiteUrl' => $bar->getWebsiteUrl(),
			'note' => $bar->getNote(),
			'openingTimes' => $openingTimesArray,
			'happyHours' => $happyHoursArray
		);
	}

	return $app->json($responseData);
})->bind('bars');

// Get one bar
$app->get('/api/bar/{id}', function ($id) use ($app) {
	$bar = $app['dao.bar']->find($id);
	if (!isset($bar)) {
		$app->abort(404, 'Bar does not exist');
	}
	$openingTimes = $bar->getOpeningTimes();
	$openingTimesArray = array();
	foreach ($openingTimes as $day => $time) {
		if ($time)
			$time = explode(' - ', $time);
		else
			$time = ['00:00:00', '00:00:00'];
		array_push($openingTimesArray,
			array(
				'day' => $day,
				'opening_time' => $time[0],
				'closing_time' => $time[1]
			)
		);
	}
  $happyHours = $bar->gethappyHours();
  $happyHoursArray = array();
  foreach ($happyHours as $day => $time) {
    if ($time)
      $time = explode(' - ', $time);
    else
      $time = ['00:00:00', '00:00:00'];
    array_push($happyHoursArray,
      array(
        'day' => $day,
        'start_time' => $time[0],
        'end_time' => $time[1]
      )
    );
  }
	$responseData = array(
		'id' => $bar->getId(),
		'latitude' => $bar->getLatitude(),
		'longitude' => $bar->getLongitude(),
		'name' => $bar->getName(),
		'houseNumber' => $bar->getHouseNumber(),
		'street' => $bar->getStreet(),
		'postCode' => $bar->getPostcode(),
		'city' => $bar->getCity(),
		'country' => $bar->getCountry(),
		'email' => $bar->getEmail(),
		'phoneNumber' => $bar->getPhoneNumber(),
		'facebookPage' => $bar->getFacebookPage(),
		'websiteUrl' => $bar->getWebsiteUrl(),
		'note' => $bar->getNote(),
		'openingTimes' => $openingTimesArray,
		'happyHours' => $happyHoursArray
	);

	return $app->json($responseData);
});

// Get bar by range
$app->post('/api/bar/byRange', function (Request $request) use ($app) {
	$latitude = $request->request->get('latitude');
	$longitude = $request->request->get('longitude');
	$range = $request->request->get('range');
	$bars = $app['dao.bar']->findInRange($latitude, $longitude, $range);


	$responseData = array();
	foreach ($bars as $bar) {
		$openingTimes = $bar->getOpeningTimes();
		$openingTimesArray = array();
		foreach ($openingTimes as $day => $time) {
			if ($time)
				$time = explode(' - ', $time);
			else
				$time = ['00:00:00', '00:00:00'];
			array_push($openingTimesArray,
				array(
					'day' => $day,
					'opening_time' => $time[0],
					'closing_time' => $time[1]
				)
			);
		}
		$happyHours = $bar->gethappyHours();
		$happyHoursArray = array();
		foreach ($happyHours as $day => $time) {
			if ($time)
				$time = explode(' - ', $time);
			else
				$time = ['00:00:00', '00:00:00'];
			array_push($happyHoursArray,
				array(
					'day' => $day,
					'start_time' => $time[0],
					'end_time' => $time[1]
				)
			);
		}
		$responseData[] = array(
			'id' => $bar->getId(),
			'name' => $bar->getName(),
			'latitude' => $bar->getLatitude(),
			'longitude' => $bar->getLongitude(),
			'address' => $bar->getHouseNumber() . " " . $bar->getStreet(),
			'houseNumber' => $bar->getHouseNumber(),
			'street' => $bar->getStreet(),
			'postCode' => $bar->getPostcode(),
			'city' => $bar->getCity(),
			'country' => $bar->getCountry(),
			'email' => $bar->getEmail(),
			'phoneNumber' => $bar->getPhoneNumber(),
			'facebookPage' => $bar->getFacebookPage(),
			'websiteUrl' => $bar->getWebsiteUrl(),
			'note' => $bar->getNote(),
			'openingTimes' => $openingTimesArray,
			'happyHours' => $happyHoursArray
		);
	}

	return $app->json($responseData);
});

// Create bar
$app->post('/api/secured/bar/create', function (Request $request) use ($app) {
	if($app['security.authorization_checker']->isGranted('ROLE_ADMIN')){
		$bar = new Bar();
		$bar->setName($request->request->get('name'));
		$bar->setLatitude($request->request->get('latitude'));
		$bar->setLongitude($request->request->get('longitude'));
		$bar->setHouseNumber($request->request->get('houseNumber'));
		$bar->setStreet($request->request->get('street'));
		$bar->setPostcode($request->request->get('postCode'));
		$bar->setCity($request->request->get('city'));
		$bar->setCountry($request->request->get('country'));
		$bar->setEmail($request->request->get('email'));
		$bar->setPhoneNumber($request->request->get('phoneNumber'));
		$bar->setFacebookPage($request->request->get('facebookPage'));
		$bar->setWebsiteUrl($request->request->get('websiteUrl'));
		$app['dao.bar']->save($bar);

		$responseData = array(
			'latitude' => $bar->getLatitude(),
			'longitude' => $bar->getLongitude(),
			'houseNumber' => $bar->getHouseNumber(),
			'street' => $bar->getStreet(),
			'postCode' => $bar->getPostcode(),
			'city' => $bar->getCity(),
			'country' => $bar->getCountry(),
			'email' => $bar->getEmail(),
			'phoneNumber' => $bar->getPhoneNumber(),
			'facebookPage' => $bar->getFacebookPage(),
			'websiteUrl' => $bar->getWebsiteUrl()
		);
		return $app->json($responseData, 202);
	} else{
		throw new AuthorizationException($app['user.jwt.options']['language']::UNAUTHORIZED);
	}
})->bind('api_bar_add');

// Delete bar
$app->delete('/api/secured/bar/{id}', function ($id, Request $request) use ($app) {
	if($app['security.authorization_checker']->isGranted('ROLE_ADMIN')){
		$responseData = $app['dao.bar']->delete($id);
	} else{
		throw new AuthorizationException($app['user.jwt.options']['language']::UNAUTHORIZED);
	}

	return $app->json($responseData, 202);
})->bind('api_bar_delete');

// Update bar
$app->put('/api/secured/bar/{id}', function ($id, Request $request) use ($app) {
	if($app['security.authorization_checker']->isGranted('ROLE_ADMIN')){
		$bar = $app['dao.bar']->find($id);
	} else{
		throw new AuthorizationException($app['user.jwt.options']['language']::UNAUTHORIZED);
	}
	$bar->setName($request->request->get('name'));
	$bar->setLatitude($request->request->get('latitude'));
	$bar->setLongitude($request->request->get('longitude'));
	$bar->setHouseNumber($request->request->get('houseNumber'));
	$bar->setStreet($request->request->get('street'));
	$bar->setPostcode($request->request->get('postCode'));
	$bar->setCity($request->request->get('city'));
	$bar->setCountry($request->request->get('country'));
	$bar->setEmail($request->request->get('email'));
	$bar->setPhoneNumber($request->request->get('phoneNumber'));
	$bar->setFacebookPage($request->request->get('facebookPage'));
	$bar->setWebsiteUrl($request->request->get('websiteUrl'));
	$app['dao.bar']->save($bar);

	$responseData = array(
		'id' => $bar->getId(),
		'latitude' => $bar->getLatitude(),
		'longitude' => $bar->getLongitude(),
		'houseNumber' => $bar->getHouseNumber(),
		'street' => $bar->getStreet(),
		'postCode' => $bar->getPostcode(),
		'city' => $bar->getCity(),
		'country' => $bar->getCountry(),
		'email' => $bar->getEmail(),
		'phoneNumber' => $bar->getPhoneNumber(),
		'facebookPage' => $bar->getFacebookPage(),
		'websiteUrl' => $bar->getWebsiteUrl()
	);

	return $app->json($responseData, 202);
})->bind('api_bar_update');

$app->post('/api/secured/bar/horaires/{id}', function ($id, Request $request) use ($app) {
	if($app['security.authorization_checker']->isGranted('ROLE_ADMIN')){
		$bar = $app['dao.bar']->find($id);
	} else{
		throw new AuthorizationException($app['user.jwt.options']['language']::UNAUTHORIZED);
	}
	$bar->setOpeningTimes($request->request->get('openingTimes'));
	$responseData = $app['dao.bar']->saveHoraires($bar);

	return $app->json($responseData, 202);
})->bind('api_bar_save_horaires');

$app->post('/api/secured/bar/happy_hours/{id}', function ($id, Request $request) use ($app) {
	if($app['security.authorization_checker']->isGranted('ROLE_ADMIN')){
		$bar = $app['dao.bar']->find($id);
	} else{
		throw new AuthorizationException($app['user.jwt.options']['language']::UNAUTHORIZED);
	}
	$bar->setHappyHours($request->request->get('happyHours'));
	$responseData = $app['dao.bar']->saveHappyHours($bar);

	return $app->json($responseData, 202);
})->bind('api_bar_save_happy_hours');
<?php

namespace App\Routes;

use App\SimpleUserJWT\Exceptions\AuthorizationException;
use Symfony\Component\HttpFoundation\Request;
use App\Model\Commentaire;


$app->get('/api/secured/comments', function () use ($app) {
	$commentaires = $app['dao.commentaire']->findAll();
	if (!isset($commentaires)) {
		return $app->json("Il n'y a pas de commentaires", 202);
	}
	$responseData = array();
	foreach ($commentaires as $commentaire) {
		$bar = $app['dao.bar']->find($commentaire->getIdBar());
		$user = $app['user.manager']->getUser($commentaire->getIdUser());
		if ($user->getUseRealName()) {
			$userName = $user->getFirstName() . " " . $user->getLastName();
		} else {
			$userName = $user->getUsername();
		}
		$responseData[] = array(
			'id' => $commentaire->getId(),
			'id_bar' => $commentaire->getIdBar(),
			'bar_name' => $bar->getName(),
			'id_user' => $commentaire->getIdUser(),
			'user_name' => $userName,
			'commentaire' => $commentaire->getCommentaire(),
			'note' => $commentaire->getNote(),
			'date_posted' => $commentaire->getDatePosted(),
			'validated' => $commentaire->getValidated()
		);
	};

	return $app->json($responseData);
})->before(function(Request $request) use ($app) {
	if (!$app['security']->isGranted('ROLE_ADMIN', $request->get('id'))) {
		return $app->json('No content', 401);
	}
})->bind('all_commentaires');

// Get commentaires
$app->get('/api/bar/commentaire/{id}', function ($id) use ($app) {
	$commentaires = $app['dao.commentaire']->getBarCommentaires($id);
	if (!isset($commentaires)) {
		return $app->json("Il n'y a pas de commentaires", 202);
	}
	$responseData = array();
	foreach ($commentaires as $commentaire) {
			$bar = $app['dao.bar']->find($commentaire->getIdBar());
			$user = $app['user.manager']->getUser($commentaire->getIdUser());
			if ($user->getUseRealName()) {
				$userName = $user->getFirstName() . " " . $user->getLastName();
			} else {
				$userName = $user->getUsername();
			}
			$responseData[] = array(
				'id' => $commentaire->getId(),
				'id_bar' => $commentaire->getIdBar(),
				'bar_name' => $bar->getName(),
				'id_user' => $commentaire->getIdUser(),
				'user_name' => $userName,
				'commentaire' => $commentaire->getCommentaire(),
				'note' => $commentaire->getNote(),
				'date_posted' => $commentaire->getDatePosted(),
				'validated' => $commentaire->getValidated()
			);
	};

	return $app->json($responseData);
})->bind('bars_commentaires');

// Create commnet
$app->post('/api/secured/commentaire/create', function (Request $request) use ($app) {
	$commentaire = new Commentaire();
	$date_posted = date('Y-m-d H:i:s');
	$commentaire->setIdBar($request->request->get('id_bar'));
	$commentaire->setIdUser($app['security']->getToken()->getUser()->getId());
	$commentaire->setDatePosted($date_posted);
	$commentaire->setNote($request->request->get('note'));
	$commentaire->setCommentaire($request->request->get('commentaire'));
	$commentaire->setValidated(true);
	$app['dao.commentaire']->save($commentaire);

	$responseData = array(
		'id' => $commentaire->getId(),
		'id_bar' => $commentaire->getIdBar(),
		'id_user' => $commentaire->getIdUser(),
		'commentaire' => $commentaire->getCommentaire(),
		'note' => $commentaire->getNote(),
		'date_posted' => $commentaire->getDatePosted()
	);
	return $app->json($responseData, 202);

})->bind('api_add_commentaire');

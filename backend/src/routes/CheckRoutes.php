<?php

namespace App\Routes;

use Symfony\Component\HttpFoundation\Request;
use App\Model\User;

$app->post('/api/checkEmail', function (Request $request) use ($app) {
  $email = $request->request->get('email');
  if (!$app['dao.user']->isEmailAvailable($email))
    return $app->json(true, 200);
  return $app->json(false, 400);
});
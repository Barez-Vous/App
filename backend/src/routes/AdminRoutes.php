<?php

namespace App\Routes;

use Symfony\Component\HttpFoundation\Request;
use App\SimpleUserJWT\User;
use App\Model\Bar;


$app->get('/api/secured/admin/nbUser', function () use ($app) {
	$nbUser = $app['dao.user']->countAll();
	return $app->json($nbUser);
})->before(function(Request $request) use ($app) {
	if (!$app['security']->isGranted('ROLE_ADMIN', $request->get('id'))) {
		return $app->json('No content', 401);
	}
});

$app->get('/api/secured/admin/nbConnectWeek', function () use ($app) {
	$nbBar = $app['dao.user']->countConnectWeek();
	return $app->json($nbBar);
})->before(function(Request $request) use ($app) {
	if (!$app['security']->isGranted('ROLE_ADMIN', $request->get('id'))) {
		return $app->json('No content', 401);
	}
});

$app->get('/api/secured/admin/deactivate/{id}', function ($id) use ($app) {
	$user = $app['user.manager']->getUser($id);
	$user->setEnabled(false);
	$app['user.manager']->update($user);
	return $app->json($user->isEnabled());
})->before(function(Request $request) use ($app) {
	if (!$app['security']->isGranted('ROLE_ADMIN', $request->get('id'))) {
		return $app->json('No content', 401);
	}
});

$app->get('/api/secured/admin/activate/{id}', function ($id) use ($app) {
	$user = $app['user.manager']->getUser($id);
	$user->setEnabled(true);
	$app['user.manager']->update($user);
	return $app->json($user->isEnabled());
})->before(function(Request $request) use ($app) {
	if (!$app['security']->isGranted('ROLE_ADMIN', $request->get('id'))) {
		return $app->json('No content', 401);
	}
});

$app->get('/api/secured/admin/nbBar', function () use ($app) {
	$nbBar = $app['dao.bar']->countAll();
	return $app->json($nbBar);
})->before(function(Request $request) use ($app) {
	if (!$app['security']->isGranted('ROLE_ADMIN', $request->get('id'))) {
		return $app->json('No content', 401);
	}
});

$app->get('/api/secured/admin/validate_comment/{id}', function ($id) use ($app) {
	$comment = $app['dao.commentaire']->find($id);
	$comment->setValidated(true);
	$app['dao.commentaire']->save($comment);
	return $app->json($comment->getValidated());
})->before(function(Request $request) use ($app) {
	if (!$app['security']->isGranted('ROLE_ADMIN', $request->get('id'))) {
		return $app->json('No content', 401);
	}
});

$app->get('/api/secured/admin/delete_comment/{id}', function ($id) use ($app) {
	$comment = $app['dao.commentaire']->find($id);
	$comment->setValidated(false);
	$app['dao.commentaire']->save($comment);
	return $app->json($comment->getValidated());
})->before(function(Request $request) use ($app) {
	if (!$app['security']->isGranted('ROLE_ADMIN', $request->get('id'))) {
		return $app->json('No content', 401);
	}
});

$app->get('/api/secured/admin/validate_bar_proposition/{id}', function ($id) use ($app) {
	$barProposition = $app['dao.barProposition']->find($id);
	if ($barProposition->getValidated() == 0) {
		if ($barProposition->getIdBar() == '') {
			$bar = new Bar();
		} else {
			$bar = $app['dao.bar']->find($barProposition->getIdBar());
		}
		$bar->setName($barProposition->getName());
		$bar->setLatitude($barProposition->getLatitude());
		$bar->setLongitude($barProposition->getLongitude());
		$bar->setHouseNumber($barProposition->getHouseNumber());
		$bar->setStreet($barProposition->getStreet());
		$bar->setPostcode($barProposition->getPostcode());
		$bar->setCity($barProposition->getCity());
		$bar->setCountry($barProposition->getCountry());
		$bar->setEmail($barProposition->getEmail());
		$bar->setPhoneNumber($barProposition->getPhoneNumber());
		$bar->setFacebookPage($barProposition->getFacebookPage());
		$bar->setWebsiteUrl($barProposition->getWebsiteUrl());
		$app['dao.bar']->save($bar);
		$barProposition->setIdBar($bar->getId());
	}
	$barProposition->setValidated(true);
	$barProposition->setDateValidated();
	$app['dao.barProposition']->save($barProposition);
	$app['dao.barProposition']->validateHappyHours($barProposition);
	$app['dao.barProposition']->validateHoraires($barProposition);
	return $app->json($barProposition->getValidated());
})->before(function(Request $request) use ($app) {
	if (!$app['security']->isGranted('ROLE_ADMIN', $request->get('id'))) {
		return $app->json('No content', 401);
	}
});

$app->get('/api/secured/admin/delete_bar_proposition/{id}', function ($id) use ($app) {
	$barProposition = $app['dao.barProposition']->find($id);
	$app['dao.barProposition']->delete($barProposition);
	return $app->json($barProposition->getValidated());
})->before(function(Request $request) use ($app) {
	if (!$app['security']->isGranted('ROLE_ADMIN', $request->get('id'))) {
		return $app->json('No content', 401);
	}
});
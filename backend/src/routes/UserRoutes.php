<?php

namespace App\Routes;

use Symfony\Component\HttpFoundation\Request;
use App\SimpleUserJWT\User;

// Get all users
$app->get('/api/secured/users', function () use ($app) {
	$users = $app['user.manager']->findBy(array(), array());
	$responseData = array();
	foreach ($users as $user) {
		$responseData[] = array(
			'id' => $user->getId(),
			'username' => $user->getUsername(),
			'email' => $user->getEmail(),
			'isEnabled' => $user->isEnabled(),
			'firstName' => $user->getFirstname(),
			'lastName' => $user->getLastName(),
			'lastLogin' => $user->getLastLogin(),
			'dateCreated' => $user->getDateCreated(),
			'picture' => $user->getPicture(),
			'roles' => $user->getRoles()
		);
	}

	return $app->json($responseData);
})->bind('users');

// Get one user
$app->get('/api/user/{id}', function ($id, Request $request) use ($app) {
	$user = $app['user.manager']->getUser($id);
	if (!isset($user)) {
		$app->abort(404, 'User not exist');
	}

	$responseData = array(
		'id' => $user->getId(),
		'username' => $user->getUsername(),
		'email' => $user->getEmail(),
		'firstName' => $user->getFirstname(),
		'lastName' => $user->getLastName(),
		'useRealName' => $user->getUseRealName(),
		'picture' => $user->getPicture(),
		'roles' => $user->getRoles()
	);

	return $app->json($responseData);
})->bind('api_user');

// Create user
$app->post('/api/user/register', function (Request $request) use ($app) {
	if (!$request->request->has('email')) {
		return $app->json("Le paramètre e-mail est manquant", 400);
	}
	if (!$request->request->has('email_confirmation')) {
		return $app->json("Le paramètre confirmation e-mail est manquant", 400);
	}
	if (!$request->request->has('password')) {
		return $app->json("Le paramètre mot de passe est manquant", 400);
	}
	if (!$request->request->has('password_confirmation')) {
		return $app->json("Le paramètre confirmation mot de passe est manquant", 400);
	}
	$email = $request->request->get('email');
	$email_confirmation = $request->request->get('email_confirmation');
	$password = $request->request->get('password');
	$password_confirmation = $request->request->get('password_confirmation');

	if (!$app['functions.global']->checkConfirmation($password, $password_confirmation)) {
		return $app->json("Vos mot de passes ne sont pas identiques !", 400);
	}
	if (!$app['functions.global']->checkConfirmation($email, $email_confirmation)) {
		return $app->json("Vos adresses e-mail ne sont pas identiques !", 400);
	}
	if (!$app['dao.user']->isEmailAvailable($email)) {
		return $app->json("Un compte avec cette adresse e-mail existe déjà !", 400);
	}

	$user = new User();
	$user->setEmail($email);
	$user->setPassword($password);
	$user->setDateCreated($app['functions.global']->getDatetimeNow());
	$app['dao.user']->save($user);

	$responseData = array(
		'id' => $user->getId(),
		'email' => $user->getEmail(),
	);

	return $app->json($responseData, 201);
})->bind('api_user_register');

// Delete user
$app->delete('/api/user/delete/{id}', function ($id, Request $request) use ($app) {
	$app['dao.user']->delete($id);

	return $app->json('No content', 204);
})->bind('api_user_delete');

// Update user
$app->put('/api/user/update/{id}', function ($id, Request $request) use ($app) {
	$user = $app['dao.user']->find($id);

	$user->setFirstname($request->request->get('firstName'));
	$user->setlastname($request->request->get('lastName'));
	$app['dao.user']->save($user);

	$responseData = array(
		'id' => $user->getId(),
		'firstName' => $user->getFirstname(),
		'lastName' => $user->getLastname()
	);

	return $app->json($responseData, 202);
})->bind('api_user_update');

$app->get('/logout', function() {
})->bind('user.logout');

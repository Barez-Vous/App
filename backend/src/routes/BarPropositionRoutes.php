<?php 
namespace App\Routes;

use App\SimpleUserJWT\Exceptions\AuthorizationException;
use Symfony\Component\HttpFoundation\Request;
use App\Model\BarProposition;

$app->get('/api/bars_proposition', function () use ($app) {
	$bars = $app['dao.barProposition']->findAll();

	$responseData = array();
	foreach ($bars as $bar) {
		$openingTimes = $bar->getOpeningTimes();
		$openingTimesArray = array();
		foreach ($openingTimes as $day => $time) {
			if ($time)
				$time = explode(' - ', $time);
			else
				$time = ['00:00:00', '00:00:00'];
			array_push($openingTimesArray,
				array(
					'day' => $day,
					'opening_time' => $time[0],
					'closing_time' => $time[1]
				)
			);
		}
		$happyHours = $bar->gethappyHours();
		$happyHoursArray = array();
		foreach ($happyHours as $day => $time) {
			if ($time)
				$time = explode(' - ', $time);
			else
				$time = ['00:00:00', '00:00:00'];
			array_push($happyHoursArray,
				array(
					'day' => $day,
					'start_time' => $time[0],
					'end_time' => $time[1]
				)
			);
		}
		$responseData[] = array(
			'id' => $bar->getId(),
			'name' => $bar->getName(),
			'latitude' => $bar->getLatitude(),
			'longitude' => $bar->getLongitude(),
			'address' => $bar->getHouseNumber() . " " . $bar->getStreet(),
			'houseNumber' => $bar->getHouseNumber(),
			'street' => $bar->getStreet(),
			'postCode' => $bar->getPostcode(),
			'city' => $bar->getCity(),
			'country' => $bar->getCountry(),
			'email' => $bar->getEmail(),
			'phoneNumber' => $bar->getPhoneNumber(),
			'facebookPage' => $bar->getFacebookPage(),
			'websiteUrl' => $bar->getWebsiteUrl(),
            'idUser' => $bar->getIdUser(),
            'datePosted' => $bar->getDatePosted(),
            'validated' => $bar->getValidated(),
            'dateValidated' => $bar->getdateValidated(),
            'isProposition' => true,
			'openingTimes' => $openingTimesArray,
			'happyHours' => $happyHoursArray
		);
	}
	return $app->json($responseData);
})->bind('bars_proposition');


// Get one bar
$app->get('/api/bar_proposition/{id}', function ($id) use ($app) {
	$bar = $app['dao.barProposition']->find($id);
	if (!isset($bar)) {
		$app->abort(404, 'Bar does not exist');
	}
	$openingTimes = $bar->getOpeningTimes();
	$openingTimesArray = array();
	foreach ($openingTimes as $day => $time) {
		if ($time)
			$time = explode(' - ', $time);
		else
			$time = ['00:00:00', '00:00:00'];
		array_push($openingTimesArray,
			array(
				'day' => $day,
				'opening_time' => $time[0],
				'closing_time' => $time[1]
			)
		);
	}
  $happyHours = $bar->gethappyHours();
  $happyHoursArray = array();
  foreach ($happyHours as $day => $time) {
    if ($time)
      $time = explode(' - ', $time);
    else
      $time = ['00:00:00', '00:00:00'];
    array_push($happyHoursArray,
      array(
        'day' => $day,
        'start_time' => $time[0],
        'end_time' => $time[1]
      )
    );
  }
	$responseData = array(
		'id' => $bar->getId(),
		'latitude' => $bar->getLatitude(),
		'longitude' => $bar->getLongitude(),
		'name' => $bar->getName(),
		'houseNumber' => $bar->getHouseNumber(),
		'street' => $bar->getStreet(),
		'postCode' => $bar->getPostcode(),
		'city' => $bar->getCity(),
		'country' => $bar->getCountry(),
		'email' => $bar->getEmail(),
		'phoneNumber' => $bar->getPhoneNumber(),
		'facebookPage' => $bar->getFacebookPage(),
		'websiteUrl' => $bar->getWebsiteUrl(),
		'openingTimes' => $openingTimesArray,
		'happyHours' => $happyHoursArray,
		'idUser' => $bar->getIdUser(),
		'datePosted' => $bar->getDatePosted(),
		'validated' => $bar->getValidated(),
		'dateValidated' => $bar->getdateValidated(),
		'isProposition' => true
	);

	return $app->json($responseData);
});

$app->post('/api/secured/bar/newProposition', function (Request $request) use ($app) {
    $bar = new BarProposition();
    $bar->setName($request->request->get('name'));
    $bar->setLatitude($request->request->get('latitude'));
    $bar->setLongitude($request->request->get('longitude'));
    $bar->setHouseNumber($request->request->get('houseNumber'));
    $bar->setStreet($request->request->get('street'));
    $bar->setPostcode($request->request->get('postCode'));
    $bar->setCity($request->request->get('city'));
    $bar->setCountry($request->request->get('country'));
    $bar->setEmail($request->request->get('email'));
    $bar->setPhoneNumber($request->request->get('phoneNumber'));
    $bar->setFacebookPage($request->request->get('facebookPage'));
    $bar->setValidated(false);
    $bar->setWebsiteUrl($request->request->get('websiteUrl'));
    $bar->setIdUser($app['security']->getToken()->getUser()->getId());
    $bar->setIdBar($bar->getId());
    $bar->setDatePosted();
    $app['dao.barProposition']->save($bar);

    $responseData = array(
		'id' => $bar->getId(),
        'latitude' => $bar->getLatitude(),
        'longitude' => $bar->getLongitude(),
        'houseNumber' => $bar->getHouseNumber(),
        'street' => $bar->getStreet(),
        'postCode' => $bar->getPostcode(),
        'city' => $bar->getCity(),
        'country' => $bar->getCountry(),
        'email' => $bar->getEmail(),
        'phoneNumber' => $bar->getPhoneNumber(),
        'facebookPage' => $bar->getFacebookPage(),
        'websiteUrl' => $bar->getWebsiteUrl(),
        'idUser' => $bar->getIdUser(),
        'datePosted' => $bar->getDatePosted()
    );
    return $app->json($responseData, 202);
})->bind('api_bar_new_proposition');

$app->post('/api/secured/bar_proposition/horaires/{id}', function ($id, Request $request) use ($app) {
	$bar = $app['dao.barProposition']->find($id);
	$bar->setOpeningTimes($request->request->get('openingTimes'));
	$responseData = $app['dao.barProposition']->saveHoraires($bar, $app['security']->getToken()->getUser()->getId());

	return $app->json($responseData, 202);
})->bind('api_bar_proposition_save_horaires');

$app->post('/api/secured/bar_proposition/happy_hours/{id}', function ($id, Request $request) use ($app) {
	$bar = $app['dao.barProposition']->find($id);
	$bar->setHappyHours($request->request->get('happyHours'));
	$responseData = $app['dao.barProposition']->saveHappyHours($bar, $app['security']->getToken()->getUser()->getId());

	return $app->json($responseData, 202);
})->bind('api_bar_proposition_save_happy_hours');

$app->put('/api/secured/bar_proposition/{id}', function ($id, Request $request) use ($app) {
	$bar = $app['dao.barProposition']->find($id);

	$bar->setName($request->request->get('name'));
	$bar->setLatitude($request->request->get('latitude'));
	$bar->setLongitude($request->request->get('longitude'));
	$bar->setHouseNumber($request->request->get('houseNumber'));
	$bar->setStreet($request->request->get('street'));
	$bar->setPostcode($request->request->get('postCode'));
	$bar->setCity($request->request->get('city'));
	$bar->setCountry($request->request->get('country'));
	$bar->setEmail($request->request->get('email'));
	$bar->setPhoneNumber($request->request->get('phoneNumber'));
	$bar->setFacebookPage($request->request->get('facebookPage'));
	$bar->setWebsiteUrl($request->request->get('websiteUrl'));
	$app['dao.barProposition']->save($bar);

	$responseData = array(
		'id' => $bar->getId(),
		'latitude' => $bar->getLatitude(),
		'longitude' => $bar->getLongitude(),
		'houseNumber' => $bar->getHouseNumber(),
		'street' => $bar->getStreet(),
		'postCode' => $bar->getPostcode(),
		'city' => $bar->getCity(),
		'country' => $bar->getCountry(),
		'email' => $bar->getEmail(),
		'phoneNumber' => $bar->getPhoneNumber(),
		'facebookPage' => $bar->getFacebookPage(),
		'websiteUrl' => $bar->getWebsiteUrl()
	);

	return $app->json($responseData, 202);
})->bind('api_bar_proposition_update');


$app->post('/api/secured/bar_proposition_happy_hours/{id}', function ($id, Request $request) use ($app) {
	$bar = $app['dao.bar']->find($id);
	$barProposition = new BarProposition();
    $barProposition->setName($bar->getName());
    $barProposition->setLatitude($bar->getLatitude());
    $barProposition->setLongitude($bar->getLongitude());
    $barProposition->setHouseNumber($bar->getHouseNumber());
    $barProposition->setStreet($bar->getStreet());
    $barProposition->setPostcode($bar->getPostcode());
    $barProposition->setCity($bar->getCity());
    $barProposition->setCountry($bar->getCountry());
    $barProposition->setEmail($bar->getEmail());
    $barProposition->setPhoneNumber($bar->getPhoneNumber());
    $barProposition->setFacebookPage($bar->getFacebookPage());
    $barProposition->setWebsiteUrl($bar->getWebsiteUrl());
    $barProposition->setValidated(0);
    $barProposition->setIdUser($app['security']->getToken()->getUser()->getId());
    $barProposition->setIdBar($bar->getId());
    $barProposition->setDatePosted();
	$barProposition->setHappyHours($request->request->get('happyHours'));
    $app['dao.barProposition']->save($barProposition);
	$responseData = $app['dao.barProposition']->saveHappyHours($barProposition, $app['security']->getToken()->getUser()->getId());

	return $app->json($responseData, 202);
})->bind('api_bar_proposition_save_happy_hours');

$app->post('/api/secured/bar_proposition_horaires/{id}', function ($id, Request $request) use ($app) {
	$bar = $app['dao.bar']->find($id);
	$barProposition = new BarProposition();
    $barProposition->setName($bar->getName());
    $barProposition->setLatitude($bar->getLatitude());
    $barProposition->setLongitude($bar->getLongitude());
    $barProposition->setHouseNumber($bar->getHouseNumber());
    $barProposition->setStreet($bar->getStreet());
    $barProposition->setPostcode($bar->getPostcode());
    $barProposition->setCity($bar->getCity());
    $barProposition->setCountry($bar->getCountry());
    $barProposition->setEmail($bar->getEmail());
    $barProposition->setPhoneNumber($bar->getPhoneNumber());
    $barProposition->setFacebookPage($bar->getFacebookPage());
    $barProposition->setWebsiteUrl($bar->getWebsiteUrl());
    $barProposition->setValidated(0);
    $barProposition->setIdUser($app['security']->getToken()->getUser()->getId());
    $barProposition->setIdBar($bar->getId());
    $barProposition->setDatePosted();
	$barProposition->setOpeningTimes($request->request->get('openingTimes'));
    $app['dao.barProposition']->save($barProposition);
	$responseData = $app['dao.barProposition']->saveHoraires($barProposition, $app['security']->getToken()->getUser()->getId());

	return $app->json($responseData, 202);
})->bind('api_bar_proposition_save_horaires');
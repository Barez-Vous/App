export interface Bar {
  id: number;
  name: string;
  latitude: string;
  longitude: string;
  houseNumber: string;
  street: string;
  postCode: string;
  city: string;
  country: string;
  email: string;
  phoneNumber: string;
  facebookPage: string;
  websiteUrl: string;
  openingTimes: [string];
  happyHours: [string];
  note: number;
  id_user: number;
  id_bar: number;
  date_posted: string;
  date_validated: string;
  validated: boolean;
  isProposition: boolean;
}

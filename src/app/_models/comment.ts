export interface Comment {
  id: number;
  id_bar: number;
  bar_name: string;
  id_user: number;
  user_name: string;
  note: number;
  commentaire: string;
  date_posted: string;
  validated: boolean;
}

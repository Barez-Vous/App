import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './_components/home/home.component';
import { LoginComponent } from './_components/login/login.component';
import { RegisterComponent } from './_components/register/register.component';
import { UserProfilComponent } from './_components/user-profil/user-profil.component';
import { AuthGuard, AdminGuard } from './_guards/index';
import { ListeBarComponent } from './_components/listeBar/listeBar.component';
import { DetailsBarComponent } from './_components/detailsBar/detailsBar.component';


import { AdminDashboardComponent } from './_admin/admin-dashboard/admin-dashboard.component';
import { UserListComponent } from './_admin/user-list/user-list.component';
import { BarListComponent } from './_admin/bar-list/bar-list.component';
import { BarDetailsComponent } from './_admin/bar-details/bar-details.component';
import { UserUpdateComponent } from './_components/user-update/user-update.component';
import { AddBarComponent } from './_components/add-bar/add-bar.component';
import { CommentListComponent } from './_admin/comment-list/comment-list.component';
import { AdminValidationComponent } from './_admin/admin-validation/admin-validation.component';

const appRoutes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'profile/:id', component: UserProfilComponent},
    { path: 'listeBar', component: ListeBarComponent },
    { path: 'bar/:barId', component: DetailsBarComponent},
    { path: 'updateProfil', component: UserUpdateComponent, canActivate: [AuthGuard]},
    { path: 'addBar/:id', component: AddBarComponent, canActivate: [AuthGuard]},

    // Admin Routes
    { path: 'admin', component: AdminDashboardComponent, canActivate: [AdminGuard]},
    { path: 'admin/users', component: UserListComponent, canActivate: [AdminGuard]},
    { path: 'admin/bars', component: BarListComponent, canActivate: [AdminGuard]},
    { path: 'admin/bar/:id', component: BarDetailsComponent, canActivate: [AdminGuard]},
    { path: 'admin/bar/:id/:isProposition', component: BarDetailsComponent, canActivate: [AdminGuard]},
    { path: 'admin/comments', component: CommentListComponent, canActivate: [AdminGuard]},
    { path: 'admin/validation', component: AdminValidationComponent, canActivate: [AdminGuard]},
    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);

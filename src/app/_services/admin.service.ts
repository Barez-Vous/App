import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { User } from '../_models/index';
import { AuthenticationService } from './authentication.service';

@Injectable()
export class AdminService {

  private baseUrl = 'http://spielmannromain.fr/BarezVous/web';

  constructor(private http: Http,
              public authenticationService: AuthenticationService) { }


  private getHeaders(){
    // I included these headers because otherwise FireFox
    // will request text/html instead of application/json
    const headers = new Headers({ 'Content-Type': 'application/json' });
    headers.append('Accept', 'application/json');
      const currentUser = this.authenticationService.getCurrentUser();
      if (currentUser && currentUser.token) {
          headers.append( 'X-Access-Token', currentUser.token );
      }
    return headers;
  }

  getNbUser(): Observable<number> {
    return this.http
      .get(`${this.baseUrl}/api/secured/admin/nbUser`, {headers: this.getHeaders()})
      .map(this.mapNumber);
  }

  getNbBar(): Observable<number> {
    return this.http
      .get(`${this.baseUrl}/api/secured/admin/nbBar`, {headers: this.getHeaders()})
      .map(this.mapNumber);
  }

  getNbConnectWeek(): Observable<number> {
    return this.http
      .get(`${this.baseUrl}/api/secured/admin/nbConnectWeek`, {headers: this.getHeaders()})
      .map(this.mapNumber);
  }

  deactivateUser(id: number): Observable<boolean> {
    return this.http
      .get(`${this.baseUrl}/api/secured/admin/deactivate/${id}`, {headers: this.getHeaders()})
      .map(this.mapBoolean);
  }

  activateUser(id: number): Observable<boolean> {
    return this.http
      .get(`${this.baseUrl}/api/secured/admin/activate/${id}`, {headers: this.getHeaders()})
      .map(this.mapBoolean);
  }

  deleteComment(id: number): Observable<boolean> {
    return this.http
      .get(`${this.baseUrl}/api/secured/admin/delete_comment/${id}`, {headers: this.getHeaders()})
      .map(this.mapBoolean);
  }

  validateComment(id: number): Observable<boolean> {
    return this.http
      .get(`${this.baseUrl}/api/secured/admin/validate_comment/${id}`, {headers: this.getHeaders()})
      .map(this.mapBoolean);
  }

  deleteBar(id: number): Observable<boolean> {
    return this.http
      .get(`${this.baseUrl}/api/secured/admin/delete_bar_proposition/${id}`, {headers: this.getHeaders()})
      .map(this.mapBoolean);
  }

  validateBar(id: number): Observable<boolean> {
    return this.http
      .get(`${this.baseUrl}/api/secured/admin/validate_bar_proposition/${id}`, {headers: this.getHeaders()})
      .map(this.mapBoolean);
  }

  mapNumber(response: Response): number {
    return response.json();
  }

  mapBoolean(response: Response): boolean {
    return response.json();
  }
}

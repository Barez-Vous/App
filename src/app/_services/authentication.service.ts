import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import {Subject} from 'rxjs/Subject';

@Injectable()
export class AuthenticationService {
    private baseUrl: string = 'http://spielmannromain.fr/BarezVous/web';

    private getHeaders(){
      // I included these headers because otherwise FireFox
      // will request text/html instead of application/json
      let headers = new Headers({ "Content-Type": "application/json" });
      headers.append('Accept', 'application/json');
        let currentUser = this.getCurrentUser();
        if (currentUser && currentUser.token) {
            headers.append( 'X-Access-Token', currentUser.token );
        }
      return headers;
    }

    constructor(private http: Http) { }

    login(email: string, password: string) {
        return this.http.post(`${this.baseUrl}/login`, JSON.stringify({ email: email, password: password }), {headers: this.getHeaders()})
            .map((response: Response) => {
                // login successful if there's a jwt token in the response
                let user = response.json();
                if (user && user.token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                }
                return user;
            });
    }

    // Observable string sources
    private componentMethodCallSource = new Subject<any>();

    // Observable string streams
    logoutMethodCalled$ = this.componentMethodCallSource.asObservable();

    // Service message commands
    logout() {
        localStorage.removeItem('currentUser');
        this.componentMethodCallSource.next();
    }

    getCurrentUser() {
        return JSON.parse(localStorage.getItem('currentUser'));
    }
}

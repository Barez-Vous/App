import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router'

import { ModalDirective } from 'angular-bootstrap-md';
import { User } from '../../_models/user';
import { UserService } from '../../_services/index';
import { ToasterService } from 'angular2-toaster';

@Component({
  selector: 'registerModal',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
  model: any = {};
  loading = false;

  constructor(
    private router: Router,
    private userService: UserService,
    private toasterService: ToasterService) {}

  register() {
    this.loading = true;
    this.userService.create(this.model)
      .subscribe(
        data => {
          this.toasterService.pop('success', 'Compte créé', 'Votre compte à bien été créé, vous pouvez vous connecter !');
          this.loading = false;
          this.hideModal();
        },
        error => {
          if(error.status == 400) {
            this.toasterService.pop('error', 'Erreur', error._body.replace(/['"]+/g,''));
          } else {
              this.toasterService.pop('error', 'Erreur', "Votre compte n'a pas été créé, veuillez réessayer.");
          }
          this.loading = false;
        });
  }

  @ViewChild('RegisterModal') public RegisterModal:ModalDirective;
  public isModalShown:boolean = false;

  public showModal():void {
      this.isModalShown = true;
  }

  public hideModal():void {
      this.RegisterModal.hide();
  }

  public onHidden():void {
      this.isModalShown = false;
  }
}

import { AuthenticationService } from './../../_services/authentication.service';
import { User } from './../../_models/user';
import { Observable } from 'rxjs/Observable';
import { Bar } from './../../_models/bar';
import { BarService } from './../../_services/index';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms/src/directives/ng_form';
import { ToasterService } from 'angular2-toaster';


@Component({
    selector: 'app-detailsBar',
    templateUrl: './detailsBar.component.html',
    styleUrls: ['./detailsBar.component.scss']
})
export class DetailsBarComponent implements OnInit {
    require: any;
    barId: number;
    noteComment: number;
    model: any = {};
    private sub: any;
    bar: Bar;
    barComments: {};
    location = {};
    currentUser: User;
    //Map pour faire le lien entre la valeur de la bdd et l'affichage
    mapJour = new Map([
        ["mon", "Lundi"],
        ["tue", "Mardi"],
        ["wed", "Mercredi"],
        ["thu", "Jeudi"],
        ["fri", "Vendredi"],
        ["sat", "Samedi"],
        ["sun", "Dimanche"]
    ]);
    //Icone Bar'ez-Vous! pour la carte
    private iconMapBV = {
        url: '../../../assets/img/LogoBarezVous.png',
        scaledSize: {
            height: 50,
            width: 50
        }
    };


    constructor(private route: ActivatedRoute,
        private barService: BarService,
        private authenticationService: AuthenticationService,
        private toasterService: ToasterService) { }

    ngOnInit() {
        this.currentUser = this.authenticationService.getCurrentUser();
        //Recuperation de la position de l'utilisateur
        if (window.navigator && window.navigator.geolocation) {
            window.navigator.geolocation.getCurrentPosition(
                position => {
                    //Quand on a le callback de la postion on recupere l'id dans le lien
                    this.location = position.coords;
                    this.sub = this.route.params.subscribe(params => {
                        this.barId = +params['barId'];
                        //On recupere les infos du bar par son id
                        this.barService.get(this.barId)
                            .subscribe(
                            data => {
                                console.log("success");
                                this.bar = data;
                                //On recupere les commentaires liés a ce bar
                                this.barService.getComments(this.barId)
                                    .subscribe(
                                    data => {
                                        console.log(data);
                                        this.barComments = data;
                                    },
                                    error => {
                                        console.log("Erreur numéro : " + error.status);
                                    });
                            },
                            error => {
                                console.log("Erreur numéro : " + error.status);
                            });
                    });
                },
                error => {
                    switch (error.code) {
                        case 1:
                            console.log('Permission Denied');
                            break;
                        case 2:
                            console.log('Position Unavailable');
                            break;
                        case 3:
                            console.log('Timeout');
                            break;
                    }
                }
            );
        };

    }

    //Conversion d'une string en number
    ConvertStringToNumber(value) {
        return parseFloat(value)
    }

    //Fonction effectuée sur l'appui du bouton d'ajout de commentaire
    postReview() {
        if (this.model.commentaire == "") {
            console.log("Commentaire vide");
            return;
        }
        //On lie le commentaire au bar
        this.model.id_bar = this.bar.id;
        this.model.note = this.noteComment;
        //Envoie du commentaire a l'API
        this.barService.addComments(this.model).subscribe(
            data => {
                this.ngOnInit();
                this.model = {};
                this.toasterService.pop('success', 'Commentaire enregistré', "Votre commentaire a bien été enregistré !");
            },
            error => {
                console.log("Erreur numéro : " + error.status);
            });

    }

    //Fonction effectué sur le changement de note sur un commentaire
    changeStar(note: number) {
        this.noteComment = note;
    }

}

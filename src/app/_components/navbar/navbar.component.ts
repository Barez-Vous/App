import { Component, OnInit } from '@angular/core';

import { User } from '../../_models/index';
import { UserService, AuthenticationService } from '../../_services/index';
import { ToasterService } from 'angular2-toaster';
import { Router } from '@angular/router';

@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})

export class NavbarComponent implements OnInit {
  currentUser: User;
  isAdmin: boolean = false;

  constructor(private userService: UserService,
    private authenticationService: AuthenticationService,
    private toasterService: ToasterService,
    private router: Router) {
    this.authenticationService.logoutMethodCalled$.subscribe(
      () => {
        this.ngOnInit();
        this.toasterService.pop('info', 'Deconnecté', 'Vous êtes bien deconnecté.');
      }
    );
  }

  ngOnInit() {
    this.isAdmin = false;
    this.currentUser = this.authenticationService.getCurrentUser();
    if (this.currentUser != null) {
      if (this.currentUser.roles.includes('ROLE_ADMIN')) {
        this.isAdmin = true;
      }
    }
  }

  logout() {
    this.authenticationService.logout();
  }

  addBar() {
    this.router.navigate(['/addBar/-1']);
  }
}

import { Observable } from 'rxjs/Observable';
import { Bar } from './../../_models/bar';
import { BarService } from './../../_services/index';
import { Component, OnInit } from '@angular/core';
import { Ng2SearchPipeModule } from 'ng2-search-filter';


@Component({
    selector: 'app-listeBar',
    templateUrl: './listeBar.component.html',
    styleUrls: ['./listeBar.component.scss']
})
export class ListeBarComponent implements OnInit {
    location = {};
    require: any;
    listeBars: Bar[];
    bars : Bar[];
    //Fausse array utilisée pour effectuer un nombre d'iteration défini dans le HTML
    fakeArray = new Array(5);
    //Icone Bar'ez-Vous! pour la carte
    private iconMapBV = {
        url: '../../../assets/img/LogoBarezVous.png',
        scaledSize: {
          height: 50,
          width: 50
        }
      };

    constructor(private barService: BarService, ) { }

    ngOnInit() {
        //Recuperation de la liste des bars
        this.barService
        .getAllAsJSON()
        .subscribe(bars => {
          this.bars = bars;
        });
        //Recuperation de la position de l'utilisateur
        if (window.navigator && window.navigator.geolocation) {
            window.navigator.geolocation.getCurrentPosition(
                position => {
                    this.location = position.coords;
                    //On applique a la recherche une portée en km, ici 10 km autour de l'utilisateur
                    this.barService.getByRange(position.coords.latitude, position.coords.longitude, "10")
                        .subscribe(
                        data => {
                            console.log("success");
                            this.listeBars = data;
                        },
                        error => {
                            console.log("Erreur numéro : " + error.status);
                        });
                },
                error => {
                    switch (error.code) {
                        case 1:
                            console.log('Permission Denied');
                            break;
                        case 2:
                            console.log('Position Unavailable');
                            break;
                        case 3:
                            console.log('Timeout');
                            break;
                    }
                }
            );
        };
    }

    //Conversion d'une string en number
    ConvertStringToNumber(value) {
        return parseFloat(value)
    }

}

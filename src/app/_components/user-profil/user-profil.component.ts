import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';

import { User } from '../../_models/index';
import { UserService, AuthenticationService } from '../../_services/index';

@Component({
  selector: 'user-profil',
  templateUrl: './user-profil.component.html',
  styleUrls: ['./user-profil.component.scss']
})
export class UserProfilComponent implements OnInit {
  user: User;
  currentUser: User;
  sub: any;

  constructor(private userService: UserService,
              private authenticationService: AuthenticationService,
              private route: ActivatedRoute,
              private router: Router){
  }

  ngOnInit(){
      this.currentUser = this.authenticationService.getCurrentUser();
      console.log(this.currentUser);
      //Recuperation de l'id de l'utilisateur a afficher dans le lien
      this.sub = this.route.params.subscribe(params => {
        let id = Number.parseInt(params['id']);
        this.userService
            .get(id)
            .subscribe(u => this.user = u);
      });
  }
}

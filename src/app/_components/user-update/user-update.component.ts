import { ToasterService } from 'angular2-toaster';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { User } from '../../_models/index';
import { UserService, AuthenticationService } from '../../_services/index';

@Component({
  selector: 'user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.scss']
})
export class UserUpdateComponent implements OnInit {
  user: User;
  currentUser: User;
  sub: any;
  model: any;
  oldmail: string = "";
  mustReLogin: boolean = false;

  constructor(private userService: UserService,
    private authenticationService: AuthenticationService,
    private route: ActivatedRoute,
    private toasterService: ToasterService,
    private router: Router) {
  }

  ngOnInit() {
    this.mustReLogin = false;
    this.currentUser = this.authenticationService.getCurrentUser();
    //Recuperation de l'utilisateur
    this.userService
      .get(this.currentUser.id)
      .subscribe(u => {
        this.user = u;
        this.oldmail = this.user.email;
      });
  }

  //Mise a jour du profil
  updateProfil() {
    //Verification du mot de passe actuel (necessaire pour valider une modification)
    if (this.user.current_password == null || this.user.current_password == "") {
      this.toasterService.pop('error', 'Modification de profil', "Vous devez renseigner votre mot de passe pour valider la modification !");
      return;
    }
    //Verification de l'username
    if (this.user.username == "") {
      this.toasterService.pop('error', 'Modification de profil', "Le nom d'utilisateur n'est pas renseigné !");
      return;
    }
    //Verification de l'email
    if (this.user.email == "") {
      this.toasterService.pop('error', 'Modification de profil', "L'adresse mail n'est pas renseignée !");
      return;
    }
    //Dans le cas ou l'email a été modifié
    if (this.oldmail != this.user.email) {
      //Verification de l'email avec confirmation
      if (this.user.email_confirmation == "" || this.user.email_confirmation != this.user.email) {
        this.toasterService.pop('error', 'Modification de profil', "L'adresse mail de confirmation n'est pas valide !");
        return;
      }
      //Si le mail a été changé l'utilisateur est déconnecté, car c'est un identifiant
      this.mustReLogin = true;
    }
    //Verification du mot de passe
    if (this.user.password != null || this.user.password == "") {
      //Verification du mdp avec confirmation
      if (this.user.password_confirmation == "" || this.user.password_confirmation != this.user.password) {
        this.toasterService.pop('error', 'Modification de profil', "Le mot de passe de confirmation n'est pas valide !");
        return;
      }
      //Si le mdp a été changé l'utilisateur est déconnecté, car c'est un identifiant
      this.mustReLogin = true;
    }
    //Mise a jour de l'utilisateur 
    this.userService.update(this.user).subscribe(
      data => {
        //Si une valeur servant d'identifiant a été modifiée l'utilisateur est deconnecté
        if (this.mustReLogin) {
          this.authenticationService.logout();
          this.router.navigate(['/']);
          this.toasterService.pop('info', 'Deconnecté', 'Vos identifiants ont été modifiés. Merci de vous reconnecter.');
        } else {
          this.ngOnInit();
          this.toasterService.pop('succes', 'Modifications', 'Votre profil a été modifié.');
        }
      },
      error => {
        console.log("Erreur numéro : " + error.status);
      });
  }
}

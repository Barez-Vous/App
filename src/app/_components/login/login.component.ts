import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDirective } from 'angular-bootstrap-md';
import { AuthenticationService } from '../../_services/index';
import { ToasterService } from 'angular2-toaster';

@Component({
    selector: 'loginModal',
    moduleId: module.id,
    templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit{
    @Output()
    parentEvent = new EventEmitter();
    model: any = {};
    loading = false;
    returnUrl: string;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private toasterService: ToasterService) { }

    ngOnInit() {
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    callParent() {
        this.parentEvent.next();
    }
    login() {
        this.loading = true;
        //Appel du service d'authentification pour la connexion
        this.authenticationService.login(this.model.email, this.model.password)
            .subscribe(
                data => {
                    this.hideModal();
                    this.callParent();
                    this.toasterService.pop('success', 'Connecté', 'Vous êtes maintenant connecté !');
                    this.loading = false;
                },
                error => {
                    if(error.status == 400) {
                      this.toasterService.pop('error', 'Erreur', error._body.replace(/['"]+/g,''));
                    } else {
                        this.toasterService.pop('error', 'Erreur', "Vous n'avez pas réussi à vous connecter, veuillez réessayer.");
                    }
                    this.loading = false;
                });
    }

    @ViewChild('LoginModal') public LoginModal:ModalDirective;
    public isModalShown:boolean = false;

    public showModal():void {
        this.isModalShown = true;
    }

    public hideModal():void {
        this.LoginModal.hide();
    }

    public onHidden():void {
        this.isModalShown = false;
    }
}

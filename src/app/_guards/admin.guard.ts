import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { User } from '../_models/index';
import { AuthenticationService } from '../_services/index';

@Injectable()
export class AdminGuard implements CanActivate {
  currentUser: User;

  constructor(private router: Router, public authenticationService: AuthenticationService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      this.currentUser = this.authenticationService.getCurrentUser();
      if (this.currentUser && this.currentUser.roles.includes('ROLE_ADMIN')) {
        return true;
      }

    // not logged in so redirect to login page with the return url
    this.router.navigate(['/home']);
    return false;
  }
}

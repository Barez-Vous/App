import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { routing } from './app.routing';

import { AlertComponent } from './_directives/index';
import { AuthGuard, AdminGuard } from './_guards/index';

import { EqualValidator } from './_validators/index';
import { HomeComponent } from './_components/home/home.component';
import { LoginComponent } from './_components/login/login.component';
import { RegisterComponent } from './_components/register/register.component';
import { NavbarComponent } from './_components/navbar/navbar.component';

import { AgmCoreModule } from '@agm/core';
import { FooterComponent } from './_components/footer/footer.component';
import { UserProfilComponent } from './_components/user-profil/user-profil.component';

import { DetailsBarComponent } from './_components/detailsBar/detailsBar.component';
import { ListeBarComponent } from './_components/listeBar/listeBar.component';
import { AddBarComponent } from './_components/add-bar/add-bar.component';

import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { DataTablesModule } from 'angular-datatables';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { UploadModule } from '@progress/kendo-angular-upload';

// Services
import { AlertService, AuthenticationService, UserService, AdminService, BarService, CommentService } from './_services/index';

// Admin
import { AdminDashboardComponent } from './_admin/admin-dashboard/admin-dashboard.component';
import { AdminNavbarComponent } from './_admin/admin-navbar/admin-navbar.component';
import { UserListComponent } from './_admin/user-list/user-list.component';
import { UserDetailsComponent } from './_admin/user-details/user-details.component';
import { BarListComponent } from './_admin/bar-list/bar-list.component'
import { BarDetailsComponent } from './_admin/bar-details/bar-details.component';
import { BarOpeningTimeComponent } from './_admin/bar-opening-time/bar-opening-time.component';
import { BarHappyHoursComponent } from './_admin/bar-happy-hours/bar-happy-hours.component';
import { UserUpdateComponent } from './_components/user-update/user-update.component';
import { CommentListComponent } from './_admin/comment-list/comment-list.component';
import { AdminValidationComponent } from './_admin/admin-validation/admin-validation.component';

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        routing,
        ToasterModule,
        UploadModule,
        HttpClientModule,
        MDBBootstrapModule.forRoot(),
        AgmCoreModule.forRoot({
          apiKey: 'AIzaSyDb5w-i6Wg7jGbGdXMdn1Ow6I0OuqQBq2w',
          libraries : ['places']
        }),
        AngularMultiSelectModule,
        DataTablesModule,
        Ng2SearchPipeModule
    ],
    declarations: [
        AppComponent,
        AlertComponent,
        HomeComponent,
        LoginComponent,
        RegisterComponent,
        EqualValidator,
        NavbarComponent,
        FooterComponent,
        AdminDashboardComponent,
        UserListComponent,
        UserDetailsComponent,
        AdminNavbarComponent,
        UserProfilComponent,
        UserUpdateComponent,
        ListeBarComponent,
        BarListComponent,
        DetailsBarComponent,
        BarDetailsComponent,
        BarOpeningTimeComponent,
        BarHappyHoursComponent,
        CommentListComponent,
        AdminValidationComponent,
        AddBarComponent
    ],
    providers: [
        AuthGuard,
        AdminGuard,
        ToasterService,
        AuthenticationService,
        UserService,
        AdminService,
        BarService,
        CommentService
    ],
    schemas: [ NO_ERRORS_SCHEMA ],
    bootstrap: [AppComponent]
})

export class AppModule { }

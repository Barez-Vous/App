import { Component, OnInit, OnDestroy, ViewChild, ElementRef, NgZone } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router} from '@angular/router';

import { Bar } from '../../_models/index';
import { BarService } from '../../_services/index';
import { ToasterService } from 'angular2-toaster';

import { } from '@types/googlemaps';
import { AgmCoreModule, MapsAPILoader,  MouseEvent as AGMMouseEvent } from '@agm/core';

@Component({
  selector: 'admin-bar-details',
  templateUrl: './bar-details.component.html',
  styleUrls: ['./bar-details.component.scss']
})

export class BarDetailsComponent implements OnInit {
  bar: any = {};
  showModalsButton = false;
  loading = false;
  id;
  lat = 48.866667;
  long = 2.333333;

  defaultLat = this.lat;
  defaultLong = this.long;
  public searchControl: FormControl;
  @ViewChild('search') public searchElementRef: ElementRef;

  public mapStyles = [
        {
          "featureType": "poi",
          "elementType": "labels",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        }
      ];
  constructor(private route: ActivatedRoute,
              private router: Router,
              private barService: BarService,
              private toasterService: ToasterService,
              private mapsAPILoader: MapsAPILoader,
              private ngZone: NgZone){}

  ngOnInit() {
    this.defaultLat = this.lat;
    this.defaultLong = this.long;
    this.route.params.subscribe(params => {
        this.id = +params['id'];
        if (params['isProposition']) {
          if (this.id != '-1') {
            this.barService
                .getProposition(this.id)
                .subscribe(
                  data => {
                    this.bar = data;
                    this.lat = this.bar.latitude;
                    this.long = this.bar.longitude;
                    this.defaultLat = this.lat;
                    this.defaultLong = this.long;
                    this.showModalsButton = true;
                 });
          }
        } else {
          if (this.id != '-1') {
            this.barService
                .get(this.id)
                .subscribe(
                  data => {
                    this.bar = data;
                    this.lat = this.bar.latitude;
                    this.long = this.bar.longitude;
                    this.defaultLat = this.lat;
                    this.defaultLong = this.long;
                    this.showModalsButton = true;
                 });
          }
        }
    });
    // create search FormControl
    this.searchControl = new FormControl();
    // load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement);
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          // get the place result
          const place: google.maps.places.PlaceResult = autocomplete.getPlace();

          // verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          // set latitude, longitude and zoom
          this.bar.latitude = String(place.geometry.location.lat());
          this.bar.longitude = String(place.geometry.location.lng());
          this.lat = this.bar.latitude;
          this.long = this.bar.longitude;
        });
      });
    });
  }

  resetPosition() {
    this.bar.latitude = String(this.defaultLat);
    this.bar.longitude = String(this.defaultLong);
    this.lat = this.bar.latitude;
    this.long = this.bar.longitude;
  }

  ConvertStringToNumber(value) {
      return parseFloat(value);
  }

  mapClicked(event: AGMMouseEvent) {
    this.bar.latitude = String(event.coords.lat),
    this.bar.longitude = String(event.coords.lng);
  }

  saveBar() {
    this.loading = true;
    this.barService.save(this.bar)
      .subscribe(
        data => {
          this.toasterService.pop('success', 'Bar Modifié', "Le bar à bien été enregistré");
          this.loading = false;
          this.router.navigate(['/admin/bars/']);
        },
        error => {
          if(error.status == 400) {
            this.toasterService.pop('error', 'Erreur', error._body.replace(/['"]+/g,''));
          } else {
              this.toasterService.pop('error', 'Erreur', "Le bar n'a pas été enregistré, veuillez réessayer.");
          }
          this.loading = false;
        });
  }

}

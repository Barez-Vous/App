import { Component, OnInit, ViewChild } from '@angular/core';
import { Bar } from '../../_models/index';
import { AuthenticationService, BarService, AdminService } from '../../_services/index';
import { ToasterService } from 'angular2-toaster';
import { DataTableDirective } from 'angular-datatables';

import { Subject } from 'rxjs/Subject';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-validation',
  templateUrl: './admin-validation.component.html',
  styleUrls: ['./admin-validation.component.scss']
})
export class AdminValidationComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  bars: Bar[];
  dtOptions: any = {};
  isFetchingData = false;
  dtTrigger: Subject<any> = new Subject();

  constructor(private router: Router,
              private barService: BarService,
              private adminService: AdminService,
              private authenticationService:  AuthenticationService,
              private toasterService: ToasterService) { }


  public ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 25,
      processing: true,
      responsive: true,
      destroy: true
    };
    this.isFetchingData = true;
    this.barService
      .getAllPropositionAsJSON()
      .subscribe(bars => {
        this.bars = bars;
        // Calling the DT trigger to manually render the table
        this.dtTrigger.next();
        this.isFetchingData = false;
      });
  }

  showDetails(barId: number) {
    this.router.navigate(['/admin/bar/' + barId + '/1']);
  }

  deleteBar(id: number) {
    this.adminService.deleteBar(id)
    .subscribe(
      data => {
        this.toasterService.pop('success', 'Bar supprimé', "Le bar à bien été supprimé");
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.destroy();
          this.ngOnInit();
        });
      },
    );
  }

  validateBar(id: number) {
    this.adminService.validateBar(id)
    .subscribe(
      data => {
        this.toasterService.pop('success', 'Bar validé', "Le bar à bien été validé");
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.destroy();
          this.ngOnInit();
        });
      },
    );
  }
}

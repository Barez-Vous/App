import { Component, OnInit, OnDestroy, ViewChild, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import _ from "lodash";

import { ActivatedRoute, Router} from '@angular/router';
import { ModalDirective } from 'angular-bootstrap-md';
import { User, Bar } from '../../_models/index';
import { BarService, AuthenticationService } from '../../_services/index';
import { ToasterService } from 'angular2-toaster';

@Component({
  selector: 'barHappyHoursModal',
  templateUrl: './bar-happy-hours.component.html',
  styleUrls: ['./bar-happy-hours.component.scss']
})


export class BarHappyHoursComponent {
  @ViewChild('BarHappyHoursModal') public BarHappyHoursModal:ModalDirective;
  public isModalShown = false;
  @Output()
  parentEvent = new EventEmitter();
  bar: Bar;
  currentUser: User;
  loading = false;
  isNewProposition = false;
  happyHoursForm: FormGroup;
  mon = false;
  tue = false;
  wed = false;
  thu = false;
  fri = false;
  sat = false;
  sun = false;
  start_time = '00:00:00';
  end_time = '00:00:00';
  constructor(private barService: BarService,
              private authenticationService: AuthenticationService,
              private toasterService: ToasterService,
              private _fb: FormBuilder) {
  }

  callParent() {
    this.parentEvent.next();
  }

  public showModal(id: number): void {
    this.currentUser = this.authenticationService.getCurrentUser();
    this.barService
        .get(id)
        .subscribe(
          data => {
            this.bar = data;
            if (this.bar.happyHours.length > 0) {
              this.setHappyHours();
            } else {
              this.happyHoursForm = this._fb.group({
                rows: this._fb.array([this.initFormsRows()])
              });
            }
            this.isModalShown = true;
        });
  }

  public showPropositionModal(id: number, modifyProposition: boolean): void {
    this.currentUser = this.authenticationService.getCurrentUser();
    if (modifyProposition) {
      this.barService
          .getProposition(id)
          .subscribe(
            data => {
              this.bar = data;
              if (this.bar.happyHours.length > 0) {
                this.setHappyHours();
              } else {
                this.happyHoursForm = this._fb.group({
                  rows: this._fb.array([this.initFormsRows()])
                });
              }
              this.isModalShown = true;
          });
    } else {
      this.barService
          .get(id)
          .subscribe(
            data => {
              this.bar = data;
              if (this.bar.happyHours.length > 0) {
                this.setHappyHours();
              } else {
                this.happyHoursForm = this._fb.group({
                  rows: this._fb.array([this.initFormsRows()])
                });
              }
              this.isModalShown = true;
          });
      this.isNewProposition = true;
    }
  }

  setHappyHours() {
    let first = true;
    const that = this;
    const grouped = _.groupBy(this.bar.happyHours, 'start_time', 'end_time');
    _.each(
      _.sortBy(
        _.toArray(grouped), function (num) {
            return num;
      }
      ).reverse(),
      function (v) {
        that.mon = false;
        that.tue = false;
        that.wed = false;
        that.thu = false;
        that.fri = false;
        that.sat = false;
        that.sun = false;
        for (var i = v.length - 1; i >= 0; i--) {
          switch (v[i]['day']) {
            case "mon":
                that.mon = true;
                that.start_time = v[i]['start_time'];
                that.end_time = v[i]['end_time'];
              break;
            case "tue":
                that.tue = true;
                that.start_time = v[i]['start_time'];
                that.end_time = v[i]['end_time'];
              break;
            case "wed":
                that.wed = true;
                that.start_time = v[i]['start_time'];
                that.end_time = v[i]['end_time'];
              break;
            case "thu":
                that.thu = true;
                that.start_time = v[i]['start_time'];
                that.end_time = v[i]['end_time'];
              break;
            case "fri":
                that.fri = true;
                that.start_time = v[i]['start_time'];
                that.end_time = v[i]['end_time'];
              break;
            case "sat":
                that.sat = true;
                that.start_time = v[i]['start_time'];
                that.end_time = v[i]['end_time'];
              break;
            case "sun":
                that.sun = true;
                that.start_time = v[i]['start_time'];
                that.end_time = v[i]['end_time'];
              break;
          }
        };
        if (first) {
          that.happyHoursForm = that._fb.group({
            rows: that._fb.array([that.initFormsRows()]) // here
          });
          first = false;
        } else {
          that.addNewRow();
        }
      }
    );
  }

  initFormsRows() {
    return this._fb.group({
        mon: this.mon, tue: this.tue, wed: this.wed, thu: this.thu, fri: this.fri, sat: this.sat, sun: this.sun,
        start_time : this.start_time,
        end_time : this.end_time,
    });
  }

  addNewRow() {
    // control refers to your formarray
    const control = <FormArray>this.happyHoursForm.controls['rows'];
    // add new formgroup
    control.push(this.initFormsRows());
  }

  deleteRow(index: number) {
    // control refers to your formarray
    const control = <FormArray>this.happyHoursForm.controls['rows'];
    // remove the chosen row
    control.removeAt(index);
  }

  public hideModal(): void {
      this.BarHappyHoursModal.hide();
  }

  public onHidden(): void {
      this.isModalShown = false;
  }

  saveBar() {
    this.loading = false;
    this.bar.happyHours = this.happyHoursForm.controls['rows']['value'];
    this.barService
      .saveHappyHours(this.bar)
      .subscribe(
        data => {
          this.toasterService.pop('success', 'Happy hours modifiés', "Les happy hours à bien été enregistrés");
          this.loading = false;
          this.hideModal();
        },
        error => {
          if(error.status == 400) {
            this.toasterService.pop('error', 'Erreur', error._body.replace(/['"]+/g,''));
          } else {
              this.toasterService.pop('error', 'Erreur', "Les happy hours n'ont pas été modifiés, veuillez réessayer.");
          }
          this.loading = false;
        });
  }

  saveProposition() {
    this.loading = false;
    this.bar.happyHours = this.happyHoursForm.controls['rows']['value'];
    this.barService
      .savePropositionHappyHours(this.bar)
      .subscribe(
        data => {
          this.toasterService.pop('success', 'Horaires modifiées', "La proposition d'happy hours a bien été enregistré");
          this.loading = false;
          this.hideModal();
        },
        error => {
          if(error.status == 400) {
            this.toasterService.pop('error', 'Erreur', error._body.replace(/['"]+/g,''));
          } else {
            this.toasterService.pop('error', 'Erreur', "Les happy hours n'ont pas été modifiés, veuillez réessayer.");
          }
          this.loading = false;
        });
  }
}

import { Component, OnInit, OnDestroy, ViewChild, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import _ from "lodash";

import { ActivatedRoute, Router} from '@angular/router';
import { ModalDirective } from 'angular-bootstrap-md';
import { User, Bar } from '../../_models/index';
import { BarService, AuthenticationService } from '../../_services/index';
import { ToasterService } from 'angular2-toaster';

@Component({
  selector: 'barOpeningTimeModal',
  templateUrl: './bar-opening-time.component.html',
  styleUrls: ['./bar-opening-time.component.scss']
})


export class BarOpeningTimeComponent {
  @ViewChild('BarOpeningTimeModal') public BarOpeningTimeModal: ModalDirective;
  public isModalShown = false;

  @Output()
  parentEvent = new EventEmitter();
  bar: Bar;
  currentUser: User;
  loading = false;
  isNewProposition = false;
  horairesForm: FormGroup;
  mon = false;
  tue = false;
  wed = false;
  thu = false;
  fri = false;
  sat = false;
  sun = false;
  opening_time = '00:00:00';
  closing_time = '00:00:00';
  constructor(private barService: BarService,
              private authenticationService: AuthenticationService,
              private toasterService: ToasterService,
              private _fb: FormBuilder){
  }

  callParent() {
    this.parentEvent.next();
  }


  public showModal(id: number): void {
    this.currentUser = this.authenticationService.getCurrentUser();
    this.barService
        .get(id)
        .subscribe(
          data => { this.bar = data;
          if (this.bar.openingTimes.length > 0) {
            this.setHoraires();
          } else {
            this.horairesForm = this._fb.group({
              rows: this._fb.array([this.initFormsRows()])
            });
          }
          this.isModalShown = true;
        });
  }

  public showPropositionModal(id: number, modifyProposition: boolean): void {
    this.currentUser = this.authenticationService.getCurrentUser();
    if (modifyProposition) {
      this.barService
          .getProposition(id)
          .subscribe(
            data => { this.bar = data;
            if (this.bar.openingTimes.length > 0) {
              this.setHoraires();
            } else {
              this.horairesForm = this._fb.group({
                rows: this._fb.array([this.initFormsRows()])
              });
            }
            this.isModalShown = true;
          });
    } else {
      this.barService
          .get(id)
          .subscribe(
            data => { this.bar = data;
            if (this.bar.openingTimes.length > 0) {
              this.setHoraires();
            } else {
              this.horairesForm = this._fb.group({
                rows: this._fb.array([this.initFormsRows()])
              });
            }
            this.isModalShown = true;
          });
      this.isNewProposition = true;
    }
  }

  setHoraires() {
    let first = true;
    const that = this;
    const grouped = _.groupBy(this.bar.openingTimes, 'opening_time', 'closing_time');
    _.each(
      _.sortBy(
        _.toArray(grouped), function (num) {
            return num;
      }
      ).reverse(),
      function (v) {
        that.mon = false;
        that.tue = false;
        that.wed = false;
        that.thu = false;
        that.fri = false;
        that.sat = false;
        that.sun = false;
        for (let i = v.length - 1; i >= 0; i--) {
          switch (v[i]['day']) {
            case "mon":
                that.mon = true;
                that.opening_time = v[i]['opening_time'];
                that.closing_time = v[i]['closing_time'];
              break;
            case "tue":
                that.tue = true;
                that.opening_time = v[i]['opening_time'];
                that.closing_time = v[i]['closing_time'];
              break;
            case "wed":
                that.wed = true;
                that.opening_time = v[i]['opening_time'];
                that.closing_time = v[i]['closing_time'];
              break;
            case "thu":
                that.thu = true;
                that.opening_time = v[i]['opening_time'];
                that.closing_time = v[i]['closing_time'];
              break;
            case "fri":
                that.fri = true;
                that.opening_time = v[i]['opening_time'];
                that.closing_time = v[i]['closing_time'];
              break;
            case "sat":
                that.sat = true;
                that.opening_time = v[i]['opening_time'];
                that.closing_time = v[i]['closing_time'];
              break;
            case "sun":
                that.sun = true;
                that.opening_time = v[i]['opening_time'];
                that.closing_time = v[i]['closing_time'];
              break;
          }
        };
        if (first) {
          that.horairesForm = that._fb.group({
            rows: that._fb.array([that.initFormsRows()]) // here
          });
          first = false;
        } else {
          that.addNewRow();
        }
      }
    );
  }

  initFormsRows() {
    return this._fb.group({
        mon: this.mon, tue: this.tue, wed: this.wed, thu: this.thu, fri: this.fri, sat: this.sat, sun: this.sun,
        opening_time : this.opening_time,
        closing_time : this.closing_time,
    });
  }

  addNewRow() {
    // control refers to your formarray
    const control = <FormArray>this.horairesForm.controls['rows'];
    // add new formgroup
    control.push(this.initFormsRows());
  }

  deleteRow(index: number) {
    // control refers to your formarray
    const control = <FormArray>this.horairesForm.controls['rows'];
    // remove the chosen row
    control.removeAt(index);
  }

  public hideModal(): void {
      this.BarOpeningTimeModal.hide();
  }

  public onHidden(): void {
      this.isModalShown = false;
  }

  saveBar() {
    this.loading = false;
    this.bar.openingTimes = this.horairesForm.controls['rows']['value'];
    this.barService
      .saveHoraires(this.bar)
      .subscribe(
        data => {
          this.toasterService.pop('success', 'Horaires modifiées', "Les horaires ont bien été enregistrés");
          this.loading = false;
          this.hideModal();
        },
        error => {
          if(error.status == 400) {
            this.toasterService.pop('error', 'Erreur', error._body.replace(/['"]+/g,''));
          } else {
              this.toasterService.pop('error', 'Erreur', "Les horaires n'ont pas été modifiés, veuillez réessayer.");
          }
          this.loading = false;
        });
  }

  saveProposition() {
    this.loading = false;
    this.bar.openingTimes = this.horairesForm.controls['rows']['value'];
    this.barService
      .savePropositionHoraires(this.bar)
      .subscribe(
        data => {
          this.toasterService.pop('success', 'Horaires modifiées', "La proposition d'horaires a bien été enregistré");
          this.loading = false;
          this.hideModal();
        },
        error => {
          if(error.status == 400) {
            this.toasterService.pop('error', 'Erreur', error._body.replace(/['"]+/g,''));
          } else {
              this.toasterService.pop('error', 'Erreur', "Les horaires n'ont pas été modifiés, veuillez réessayer.");
          }
          this.loading = false;
        });
  }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from '../../_models/index';
import { AuthenticationService, UserService, AdminService } from '../../_services/index';
import { ToasterService } from 'angular2-toaster';
import { DataTableDirective } from 'angular-datatables';

import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'admin-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  users : User[];
  currentUser : User;
  dtOptions: DataTables.Settings = {};
  isFetchingData = false;
  dtTrigger: Subject<any> = new Subject();

  constructor(private userService : UserService,
              private adminService : AdminService,
              private authenticationService : AuthenticationService,
              private toasterService: ToasterService) { }


    public ngOnInit() {
      this.dtOptions = {
        pagingType: 'full_numbers',
        pageLength: 25,
        processing: true,
        destroy:true
      };
      this.isFetchingData = true;
      this.userService
          .getAllAsJSON()
          .subscribe(users => {
            this.users = users;
            // Calling the DT trigger to manually render the table
            this.dtTrigger.next();
            this.isFetchingData = false;
          });
      this.currentUser = this.authenticationService.getCurrentUser();
    }


  deactivateUser(id:number) {
    this.adminService.deactivateUser(id)
    .subscribe(
      data => {
        this.toasterService.pop('success', 'Utilisateur Désactivé', "L'utilisateur à bien été désactivé");
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.destroy();
          this.ngOnInit();
        });
      },
    );
  }

  activateUser(id:number) {
    this.adminService.activateUser(id)
    .subscribe(
      data => {
        this.toasterService.pop('success', 'Utilisateur Activé', "L'utilisateur à bien été activé");
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.destroy();
          this.ngOnInit();
        });
      },
    );
  }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { User, Bar} from '../../_models/index';
import { AuthenticationService, UserService, AdminService, BarService } from '../../_services/index';
import { ToasterService } from 'angular2-toaster';
import { ActivatedRoute, Router} from '@angular/router';

import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'admin-bar-list',
  templateUrl: './bar-list.component.html',
  styleUrls: []
})
export class BarListComponent implements OnInit {
  bars : Bar[];
  currentUser : User;
  dtOptions: any = {};
  isFetchingData;
  dtTrigger: Subject<any> = new Subject();
  constructor(private router:Router,
              private barService : BarService,
              private adminService : AdminService,
              private authenticationService : AuthenticationService,
              private toasterService: ToasterService) { }


  public ngOnInit() {
    let that = this;
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 25,
      destroy:true,
      dom: 'Bfrtip',
      buttons: [
        'columnsToggle',
        'copy',
        {
          text: 'Ajouter bar',
          key: '1',
          action: function (e, dt, node, config) {
            that.router.navigate(['/admin/bar/-1']);
          }
        }
      ]
    };
    this.isFetchingData = true;
    this.barService
        .getAllAsJSON()
        .subscribe(bars => {
          this.bars = bars;
          this.dtTrigger.next();
          this.isFetchingData = false;
        });
    this.currentUser = this.authenticationService.getCurrentUser();
  }

  showDetails(barId: number) {
    this.router.navigate(['/admin/bar/' + barId]);
  }

  deleteBar(bar_id:number) {
    this.barService
      .delete(bar_id)
      .subscribe(
        data => {
          this.ngOnInit();
          this.toasterService.pop('success', 'Bar supprimé', "Le bar à bien été supprimé");
        },
        error => {
          if(error.status == 400) {
            this.toasterService.pop('error', 'Erreur', error._body.replace(/['"]+/g,''));
          } else {
              this.toasterService.pop('error', 'Erreur', "Le bar n'a pas été supprimé, veuillez réessayer.");
          }
        });
  }
}

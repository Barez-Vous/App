import { Component, OnInit, OnDestroy, ViewChild, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
import { ModalDirective } from 'angular-bootstrap-md';

import { User } from '../../_models/index';
import { UserService, AuthenticationService } from '../../_services/index';
import { ToasterService } from 'angular2-toaster';

@Component({
  selector: 'userDetailsModal',
  templateUrl: './user-details.component.html',
  styles: []
})


export class UserDetailsComponent {
  @Output()
  parentEvent = new EventEmitter();
  user: User;
  currentUser : User;
  loading = false;

  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  constructor(private userService: UserService,
              private authenticationService: AuthenticationService,
              private toasterService: ToasterService){
  }

  callParent() {
    this.parentEvent.next();
  }

  @ViewChild('UserDetailsModal') public UserDetailsModal:ModalDirective;
  public isModalShown:boolean = false;

  public showModal(id:number):void {
    this.currentUser = this.authenticationService.getCurrentUser();
    this.userService
        .get(id)
        .subscribe(
          data => { this.user = data;
          this.isModalShown = true;
          this.user.roles;

          this.dropdownList = [
              {"id":1,"itemName":"ROLE_USER"},
              {"id":2,"itemName":"ROLE_REGISTERED"},
              {"id":3,"itemName":"ROLE_ADMIN"}
            ];
          this.selectedItems = [];
          if (this.user.roles.indexOf("ROLE_USER") > -1) this.selectedItems.push({"id":1,"itemName":"ROLE_USER"});
          if (this.user.roles.indexOf("ROLE_REGISTERED") > -1) this.selectedItems.push({"id":2,"itemName":"ROLE_REGISTERED"});
          if (this.user.roles.indexOf("ROLE_ADMIN") > -1) this.selectedItems.push({"id":3,"itemName":"ROLE_ADMIN"});

          this.dropdownSettings = {
            singleSelection: false,
            text:"Sélectionnez les roles",
            selectAllText:'Sélectionner tout',
            unSelectAllText:'Désélectionner tout',
            enableSearchFilter: true
          };
        });
  }

  public hideModal():void {
      this.UserDetailsModal.hide();
  }

  public onHidden():void {
      this.isModalShown = false;
  }

  saveUser() {
    for (let listKey in this.selectedItems) {
      let roleDict = this.selectedItems[listKey];
      for (let roleKey in roleDict) {
        let role = roleDict[roleKey];
        if(roleKey == "itemName") {
          this.user.roles.push(role);
        }
      }
    }
    this.loading = true;
    this.userService.save(this.user)
      .subscribe(
        data => {
          this.toasterService.pop('success', 'Utilisateur Modifié', "L'utilisateur à bien été enregistré");
          this.hideModal();
          this.callParent();
          this.loading = false;
        },
        error => {
          if(error.status == 400) {
            this.toasterService.pop('error', 'Erreur', error._body.replace(/['"]+/g,''));
          } else {
              this.toasterService.pop('error', 'Erreur', "L'utilisateur n'a pas été modifié, veuillez réessayer.");
          }
          this.loading = false;
        });
  }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { Comment } from '../../_models/index';
import { AuthenticationService, CommentService, AdminService } from '../../_services/index';
import { ToasterService } from 'angular2-toaster';
import { DataTableDirective } from 'angular-datatables';

import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'admin-comment-list',
  templateUrl: './comment-list.component.html',
  styleUrls: ['./comment-list.component.scss']
})
export class CommentListComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  comments : Comment[];
  dtOptions: any = {};
  isFetchingData = false;
  dtTrigger: Subject<any> = new Subject();

  constructor(private commentService : CommentService,
              private adminService : AdminService,
              private authenticationService : AuthenticationService,
              private toasterService: ToasterService) { }


    public ngOnInit() {
      this.dtOptions = {
        pagingType: 'full_numbers',
        pageLength: 25,
        processing: true,
        responsive: true,
        destroy:true
      };
      this.isFetchingData = true;
      this.commentService
          .getAllAsJSON()
          .subscribe(comments => {
            this.comments = comments;
            // Calling the DT trigger to manually render the table
            this.dtTrigger.next();
            this.isFetchingData = false;
          });
    }


  deleteComment(id:number) {
    this.adminService.deleteComment(id)
    .subscribe(
      data => {
        this.toasterService.pop('success', 'Commentaire supprimé', "Le commentaire à bien été supprimé");
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.destroy();
          this.ngOnInit();
        });
      },
    );
  }

  validateComment(id:number) {
    this.adminService.validateComment(id)
    .subscribe(
      data => {
        this.toasterService.pop('success', 'Commentaire validé', "Le commentaire à bien été validé");
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.destroy();
          this.ngOnInit();
        });
      },
    );
  }
}

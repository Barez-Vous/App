import { Component, OnInit } from '@angular/core';
import { User } from '../../_models/index';
import { UserService, AuthenticationService } from '../../_services/index';
import { ToasterService } from 'angular2-toaster';

import { ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'admin-navbar',
  templateUrl: './admin-navbar.component.html',
  styleUrls: ['./admin-navbar.component.scss']
})
export class AdminNavbarComponent implements OnInit {

  currentUser : User;

  constructor(private router:Router,
              private userService: UserService,
              private authenticationService: AuthenticationService,
              private toasterService: ToasterService) {
  }

  ngOnInit() {
    this.currentUser = this.authenticationService.getCurrentUser();
    if (this.currentUser != null) {
      if (!this.currentUser.roles.includes('ROLE_ADMIN')) {
        location.reload();
      }
    } else {
      location.reload();
    }
  }

  logout() {
    this.authenticationService.logout();
    location.reload();
  }
}

import { Component, OnInit } from '@angular/core';
import { AuthenticationService, AdminService } from '../../_services/index';

@Component({
  selector: 'admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.scss']
})
export class AdminDashboardComponent implements OnInit {
  nbUser: number;
  nbBar: number;
  nbConnectWeek: number;
  constructor( private adminService:AdminService ) { }

  ngOnInit() {
    this.adminService.getNbUser()
        .subscribe(u => this.nbUser = u);
    this.adminService.getNbBar()
        .subscribe(u => this.nbBar = u);
    this.adminService.getNbConnectWeek()
        .subscribe(u => this.nbConnectWeek = u);
  }
}

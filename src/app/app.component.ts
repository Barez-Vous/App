import { Component } from '@angular/core';
import { Routes, RoutesRecognized, Router } from '@angular/router';

@Component({
    moduleId: module.id,
    selector: 'app',
    templateUrl: 'app.component.html',
    styleUrls: ['./app.component.scss']
})

export class AppComponent {
  isAdminPage : boolean = false;
  constructor(private _r: Router) {
    this.isAdminPage = false;
    this._r.events.subscribe(event => {
      if (event instanceof RoutesRecognized) {
        if (event.state.toString().indexOf('admin') != -1) this.isAdminPage = true;
      }
    });
  }
}
